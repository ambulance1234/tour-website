module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      spacing: {
        1240: "1240px",
      },
      margin: {
        "5px": "5px",
      },
      height: {
        121: "121px",
      },
    },
  },
  plugins: [],
};
