export const reviewData = (data) => {
  return {
    type: "REVIEW_DATA",
    payload: data,
  };
};
