export const countriesList = (data) => {
  return {
    type: "COUNTRY_LIST",
    payload: data,
  };
};
