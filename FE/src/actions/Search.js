export const searchData = (data) => {
  return {
    type: "SEARCH_LIST",
    payload: data,
  };
};
