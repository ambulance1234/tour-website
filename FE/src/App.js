import React, { Suspense } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import "../src/assets/Styles/GlobalStyles.css";
import { Countries } from "./components/Countries";
import Footer from "./components/Footer";
import Header from "./components/Header";
import { Loading } from "./components/Loading";
import Error from "./components/NotFound";
import { AdminPage } from "./pages/Admin";
import AdminLayout from "./pages/AdminLayout";
import OrgViewDetailTour from "./pages/OrgViewDetailTour";
import OrgViewTour from "./pages/OrgViewTour";

// const Tour = React.lazy(() => import("./pages/index"));
const HomePage = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Main/index")), 1000);
  });
});

const RegistUser = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/RegistUser/index")), 1000);
  });
});

const RegistOrg = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/RegistOrg/index")), 1000);
  });
});

const Detail = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/DetailPage/index")), 1000);
  });
});

const PostPage = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/PostPage/index")), 1000);
  });
});

const RegistAd = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/RegistAd/index")), 1000);
  });
});

const Ticket = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Ticket/index")), 1000);
  });
});

const SearchResult = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./components/SearchResult/index")), 1000);
  });
});

const EditPostPage = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/EditPostPage/index")), 1000);
  });
});

function App() {
  return (
    <BrowserRouter>
      <Countries />

      <Header />

      <Routes>
        <Route path="/" element={<Navigate to="home" />} />

        <Route
          index
          path="home"
          element={
            <Suspense fallback={<Loading />}>
              <HomePage />
            </Suspense>
          }
        />

        <Route
          path="detail/:tourId/:orgId"
          element={
            <Suspense fallback={<Loading />}>
              <Detail />
            </Suspense>
          }
        />

        <Route
          path="post"
          element={
            <Suspense fallback={<Loading />}>
              <PostPage />
            </Suspense>
          }
        />

        <Route
          path="registOrg"
          element={
            <Suspense fallback={<Loading />}>
              <RegistOrg />
            </Suspense>
          }
        />

        <Route
          path="registUser"
          element={
            <Suspense fallback={<Loading />}>
              <RegistUser />
            </Suspense>
          }
        />

        <Route
          path="registAd"
          element={
            <Suspense fallback={<Loading />}>
              <RegistAd />
            </Suspense>
          }
        />

        <Route
          path="ticket/:cartId"
          element={
            <Suspense fallback={<Loading />}>
              <Ticket />
            </Suspense>
          }
        />

        <Route
          path="adminPage"
          element={
            <Suspense fallback={<Loading />}>
              <AdminPage />
            </Suspense>
          }
        />

        <Route
          path="orgViewTour"
          element={
            <Suspense fallback={<Loading />}>
              <OrgViewTour />
            </Suspense>
          }
        />

        <Route
          path="orgViewDetailTour/:tourId"
          element={
            <Suspense fallback={<Loading />}>
              <OrgViewDetailTour />
            </Suspense>
          }
        />

        <Route
          path="AdminLayout"
          element={
            <Suspense fallback={<Loading />}>
              <AdminLayout />
            </Suspense>
          }
        />

        <Route
          path="result/:keyword"
          element={
            <Suspense fallback={<Loading />}>
              <SearchResult />
            </Suspense>
          }
        />

        <Route
          path="edit-post/:tourId"
          element={
            <Suspense fallback={<Loading />}>
              <EditPostPage />
            </Suspense>
          }
        />

        <Route path="*" element={<Error />} />
      </Routes>

      <Footer />
    </BrowserRouter>
  );
}

export default App;
