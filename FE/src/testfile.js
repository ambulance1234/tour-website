function compare(str1, str2) {
  str1 = String(str1);
  str2 = String(str2);
  if(str1.length > str2.length) { return false; }
  else if(str1.length < str2.length) { return false; }
  else {
    for(let i = 0; i < str1.length; i ++) {
      if(str1[i] !== str2[i]) {
        return false;
      }
    }
    return true;
  }
}
let role = "VICE";
let cmp = true;
if(compare(role, "USER") || compare(role, "ORGANIZATION") || compare(role, "ADMIN")) {
  cmp = true;
}
else {
  cmp = false;
}

console.log(cmp);
