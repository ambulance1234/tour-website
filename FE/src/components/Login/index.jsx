import { CircularProgress } from "@mui/material";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState } from "react";
import { handleLoginSubmit } from "../../constants/api";
import {
  initialLoginValues,
  loginValidationSchema,
} from "../../constants/validate";
import Notification from "../Notification";
import "./Login.css";

Login.propTypes = {};

function Login(props) {
  const { isModal } = props;

  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  return (
    <div className={isModal ? "login" : "disable_login"}>
      <Formik
        validationSchema={loginValidationSchema}
        initialValues={initialLoginValues}
        onSubmit={(values, { setSubmitting }) => {
          handleLoginSubmit(values, { setSubmitting }, { setNotify });
        }}
      >
        {(formikProps) => {
          const { isSubmitting } = formikProps;
          return (
            <Form className="login_box">
              <div className="login_box_title">
                <p>Welcome to</p>
                <p>Tour</p>
                <p>Ads</p>
              </div>
              <div className="login_box_body">
                <Field
                  name="Email"
                  className="login_bar"
                  type="text"
                  placeholder="Email"
                />
                <ErrorMessage
                  // className={styles.error}
                  name="Email"
                  // component={FormFeedback}
                />

                <Field
                  name="Password"
                  className="login_bar"
                  type="password"
                  placeholder="Password"
                />
                <ErrorMessage
                  // className={styles.error}
                  name="Password"
                  // component={FormFeedback}
                />
              </div>
              <button
                type="submit"
                className="login_box_btn"
                // onClick={handleModal}
              >
                {isSubmitting ? (
                  <CircularProgress color="success" />
                ) : (
                  "Sign In"
                )}
              </button>
              <Notification notify={notify} />
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default Login;
