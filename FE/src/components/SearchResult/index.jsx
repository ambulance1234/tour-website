import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import { faCalendarDay, faUserAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { calculateTime, scrollTop } from "../../constants/global";
import { Images } from "../../constants/images";
import "./SearchResult.css";

function SearchResult() {
  const { keyword } = useParams();
  const navigate = useNavigate();

  var [searchList, setSearchList] = useState([]);

  useEffect(() => {
    handleSearchCountries();
  }, [keyword]);

  const handleSearchCountries = async () => {
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/search/location?location_name=${keyword}`
      );
      console.log(
        "🚀 ~ file: validate.js ~ line 47 ~ handleSearchCountries ~ res",
        res.data
      );

      searchList = res.data.message.rows;
      setSearchList(searchList);
    } catch (error) {
      if (error && error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
    }
  };

  return (
    <div className="searchResult_container">
      {searchList.length !== 0 && (
        <div className="searchResult_info">
          <p>Search results &#8220; {keyword} &#8221; </p>
          <p>{searchList.length} properties found </p>
          <a href="/home">Search more location </a>
        </div>
      )}
      <div className="searchResult_detail">
        {searchList.length === 0 && (
          <div className="searchResult_null">
            <img src={Images.EMPTY} alt="" />
            Can't find your &#8220; {keyword} &#8221;
          </div>
        )}

        {searchList.length !== 0 &&
          searchList.map((item, key) => {
            return (
              <div
                key={key}
                className="searchResult_child"
                onClick={() => {
                  navigate(`/detail/${item.tour_id}/${item.org_id}`);
                  scrollTop();
                }}
              >
                <div className="searchResult_img">
                  {/* <img src={Images.BEACH} alt="" /> */}
                  <img
                    className="carousel_item_img"
                    src={
                      `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                        ? `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                        : Images.BEACH
                    }
                    alt=""
                  />
                </div>
                <p className="name">{item.tour_name} </p>
                <div className="child_info">
                  <span>
                    <FontAwesomeIcon icon={faCalendarCheck} /> Available
                  </span>

                  <span>
                    <FontAwesomeIcon icon={faCalendarDay} />{" "}
                    {calculateTime(item.hours)}
                  </span>
                </div>
                <p className="price">$ {item.price} </p>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default SearchResult;
