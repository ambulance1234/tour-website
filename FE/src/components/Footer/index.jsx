import React from "react";
import { useSelector } from "react-redux";
import { Images } from "../../constants/images";
import "./Footer.css";
Footer.propTypes = {};

function Footer(props) {
  const ticket = useSelector((state) => state.count);

  const { ticketId } = ticket;
  function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");

    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less";
      moreText.style.display = "inline";
    }
  }

  if (window.location.pathname === `/registUser`) return <div></div>;
  if (window.location.pathname === `/registAd`) return <div></div>;
  if (window.location.pathname === `/registOrg`) return <div></div>;
  if (window.location.pathname === `/adminPage`) return <div></div>;
  if (window.location.pathname === `/ticket/${ticketId}`) return <div></div>;
  return (
    <div className="footer">
      <div className="more_info">
        <div className="block_info">
          <span className="info_header">About TourAds</span>
          <ul className="info_content">
            <li>About Us</li>
            <li>Press</li>
            <li>Resources and Policies</li>
            <li>Trust & Safety</li>
          </ul>
        </div>
        <div className="block_info">
          <span className="info_header">Explore</span>
          <ul className="info_content">
            <li>Write a review</li>
            <li>Add a Place</li>
            <li>Join </li>
            <li>Travellers' Choice</li>
            <li>GreenLeaders</li>
            <li>Help Center</li>
          </ul>
        </div>
        <div className="block_info">
          <span className="info_header">Do Business With Us</span>
          <ul className="info_content">
            <li>Owners</li>
            <li>Business Advantage</li>
            <li>Sponsored Placements</li>
            <li>Advertise with Us</li>
          </ul>
        </div>
        <div className="block_info">
          <span className="info_header">Get The App</span>
          <ul className="info_content">
            <li>iPhone App</li>
            <li>Android App</li>
          </ul>
        </div>
      </div>
      <div className="certificate">
        <div className="cert_logo">
          <img src={Images.LOGO} alt="img" />
        </div>
        <div className="cert_content">
          <span>© 2022 TourAds LLC All rights reserved.</span>
          <ul className="privacy">
            <li className="privacy_content">Terms of Use</li>
            <li className="privacy_content">Privacy and Cookies Statement</li>
            <li className="privacy_content">Cookie consent</li>
            <li className="privacy_content">Site Map</li>
            <li className="privacy_content">How the site works</li>
          </ul>
        </div>
      </div>
      <div className="notify">
        This is the version of our website addressed to speakers of English in
        the United States. If you are a resident of another country or region,
        please select the appropriate version of TourAds for your country or
        region in the drop-down menu.<span id="dots">...</span>
        <span id="more" className="moree">
          <br />
          TourAds LLC makes no guarantees for availability of prices advertised
          on our sites and applications. Listed prices may require a stay of a
          particular length or have blackout dates, qualifications or other
          applicable restrictions. TourAds LLC is not responsible for any
          content on external web sites that are not owned or operated by
          TourAds. Indicative hotel prices displayed on our “Explore” pages are
          estimates extrapolated from historic pricing data.
          <br />
          TourAds LLC is not a booking agent or tour operator. When you book
          with one of our partners, please be sure to check their site for a
          full disclosure of all applicable fees.
        </span>
        <button onClick={myFunction} id="myBtn" className="show_more">
          Read more
        </button>
      </div>
    </div>
  );
}

export default Footer;
