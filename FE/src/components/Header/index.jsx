import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "universal-cookie";
import { handleLogout } from "../../constants/api";
import { Images } from "../../constants/images";
import Login from "../Login";
import "./Header.css";
import { useDispatch, useSelector } from "react-redux";
import { ticketId } from "../../actions/Count";

Header.propTypes = {};

function Header(props) {
  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const userAccount = cookies.get("account");
  const navigate = useNavigate();
  const ticket = useSelector((state) => state.count);

  const { ticketId } = ticket;

  console.log("🚀 ~ file: index.jsx ~ line 14 ~ Header ~ userToken", userToken);
  console.log(
    "🚀 ~ file: index.jsx ~ line 15 ~ Header ~ userAccount",
    userAccount
  );

  const [isModal, setIsModal] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  if (window.location.pathname === `/registUser`) return <div></div>;
  if (window.location.pathname === `/registOrg`) return <div></div>;
  if (window.location.pathname === `/registAd`) return <div></div>;
  if (window.location.pathname === `/ticket/${ticketId}`) return <div></div>;
  // if (window.location.pathname === `/adminPage`) return <div></div>;

  const roleRecognize = (roleId) => {
    // console.log(
    //   "🚀 ~ file: index.jsx ~ line 30 ~ roleRecognize ~ roleId",
    //   roleId
    // );
    if (roleId === 1) return "Admin";
    if (roleId === 2) return "Organize";
    if (roleId === 3) return "User";
  };

  return (
    <div className="header">
      <div className="logo_name">
        <div className="logo_border">
          <img src={Images.LOGO} alt="img" />
        </div>
        <div className="logo_name_border" onClick={() => navigate("/home")}>
          <span>Tour</span>
          <span>Ads</span>
        </div>
      </div>

      {/* header khi chưa login */}
      <Login
        isLogin={isLogin}
        setIsLogin={setIsLogin}
        isModal={isModal}
        setIsModal={setIsModal}
      />
      {/* <AccountMenu isLogin={isLogin} setIsLogin={setIsLogin} /> */}
      {userToken ? (
        <div className="login_regist login_regist_after">
          {userAccount && userAccount.role_id === 2 ? (
            <button
              className="login_regist_btns"
              onClick={() => navigate(`/orgViewTour`)}
            >
              View Tour
            </button>
          ) : (
            <button className="login_regist_btns">Your Tour</button>
          )}
          <div className="user_name">
            <span>{userAccount.email}</span>
            <span>{roleRecognize(userAccount.role_id)} </span>
          </div>
          <div className="user_img_border">
            <img src={Images.JOKER} alt="" />
            <div className="user_option">
              <button type="button" onClick={handleLogout}>
                <FontAwesomeIcon icon={faRightFromBracket} /> Logout
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div className="login_regist">
          {(!userAccount || userAccount.role_id === undefined) && (
            <button className="login_regist_btns">Your Tour</button>
          )}
          <button
            onClick={() => setIsModal(true)}
            className="login_regist_btns"
          >
            Sign In
          </button>
          <button
            className="login_regist_btns sign_up-btn"
            onClick={() => navigate("registUser")}
          >
            Sign Up
          </button>
        </div>
      )}
    </div>
  );
}

export default Header;
