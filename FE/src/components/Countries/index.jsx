import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { countriesList } from "../../actions/Countries";

export const Countries = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    getCountries();
  }, []);

  const getCountries = async () => {
    try {
      const res = await axios.get("http://localhost:5000/api/countries");

      if (res.data) dispatch(countriesList(res.data.message));
    } catch (error) {
      if (error && error.response) console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };
  return <div></div>;
};
