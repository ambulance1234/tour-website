import { faCircleArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import Cookies from "universal-cookie";
import { Images } from "../../constants/images";
import "./Ticket.css";
import { useDispatch } from "react-redux";
import { ticketId } from "../../actions/Count";
import Notification from "../../components/Notification";
import { handleCancelTour } from "../../constants/api";

Ticket.propTypes = {};

function Ticket(props) {
  const { cartId } = useParams();

  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [ticketDetail, setTicketDetail] = useState(null);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  useEffect(() => {
    getTicket();
  }, []);

  useEffect(() => {
    dispatch(ticketId(cartId));
  }, [cartId]);

  const getTicket = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };
    try {
      const res = await axios.get(
        `http://localhost:5000/api/accounts/users/cart/detail?cart=${cartId}`,
        config
      );
      console.log("🚀 ~ file: index.jsx ~ line 34 ~ getTicket ~ res", res.data);
      setTicketDetail(res.data.message);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div className="ticket">
      <img className="ticket-bg" src={Images.TICKET} alt="" />
      <button onClick={() => navigate("/")} className="ticket_back_btn">
        <FontAwesomeIcon icon={faCircleArrowLeft} />
      </button>
      <div className="ticket_title_detail">
        <div className="ticket_title">
          <span>Booked Tour</span>
        </div>
        <div className="titket_detail">
          <div className="detail_header">
            <p>
              {ticketDetail && ticketDetail.tour_name
                ? ticketDetail.tour_name
                : ""}{" "}
            </p>
            <p>
              {`${
                ticketDetail && ticketDetail.location_name
                  ? ticketDetail.location_name
                  : ""
              } - ${
                ticketDetail && ticketDetail.country_name
                  ? ticketDetail.country_name
                  : ""
              }`}{" "}
            </p>
            <p>
              {ticketDetail && ticketDetail.org_name
                ? ticketDetail.org_name
                : 0}
            </p>
            <p>
              Phone:{" "}
              {ticketDetail && ticketDetail.phone ? ticketDetail.phone : ""} -
              Address:{" "}
              {ticketDetail && ticketDetail.address ? ticketDetail.address : ""}{" "}
            </p>
          </div>
          <div className="detail_body">
            <div className="detail_body_title">
              <p>Tourist</p>
              <p>Booked time</p>
              <p>Time of trip</p>
              <p>Price / person</p>
            </div>
            <div className="detail_body_content">
              <p>
                {ticketDetail && ticketDetail.number_tourists
                  ? ticketDetail.number_tourists
                  : 0}
              </p>
              <p>
                {ticketDetail && ticketDetail.booked_time
                  ? ticketDetail.booked_time.substring(0, 11)
                  : 0}
              </p>
              <p>
                {ticketDetail && ticketDetail.hours ? ticketDetail.hours : 0}{" "}
                hours
              </p>
              <p>
                ${ticketDetail && ticketDetail.price ? ticketDetail.price : 0}
              </p>
            </div>
          </div>
          <div className="detail_footer">
            <div className="detail_footer_total">
              <p>Total</p>
              <p>
                $
                {ticketDetail
                  ? ticketDetail.price * ticketDetail.number_tourists
                  : 0}{" "}
              </p>
            </div>
            <button
              onClick={() => handleCancelTour(cartId, { setNotify })}
              className="detail_footer_cancle"
            >
              Cancel this trip?
            </button>
            <Notification notify={notify} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Ticket;
