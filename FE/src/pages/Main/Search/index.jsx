import { faMagnifyingGlassLocation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import { Images } from "../../../constants/images";
import "./Search.css";

Search.propTypes = {};

function Search(props) {
  const [keyword, setKeyword] = useState("");
  const navigate = useNavigate();

  const handleSearchCountries = async (e) => {
    e.preventDefault();
    navigate(`/result/${keyword}`);
  };

  return (
    <form onSubmit={handleSearchCountries} className="search">
      <div className="img_search_border">
        <img alt="img" src={Images.BEACH} className="img_search" />
      </div>
      <div className="search_group">
        <button type="button" className="search_btn">
          <FontAwesomeIcon icon={faMagnifyingGlassLocation} size="2x" />
          <input
            name="keyword"
            className="search_bar"
            placeholder="Where do you want to make a trip?"
            onChange={(e) => setKeyword(e.target.value)}
          />
        </button>
      </div>
    </form>
  );
}

export default Search;
