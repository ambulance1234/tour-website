import React from "react";
import { useNavigate } from "react-router-dom";
import { Images } from "../../../constants/images";
import "./Banner.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { faUser, faUserGroup } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Cookies from "universal-cookie";
import { settings } from "../../../constants/global";

Banner.propTypes = {};

function Banner(props) {
  const { userCartList } = props;
  const cookies = new Cookies();
  const userAccount = cookies.get("account");
  const navigate = useNavigate();

  return (
    <div className="banner">
      {/* Banner khi chưa login */}

      <img src={Images.LONDON} className="banner_img" alt="img" />
      {userAccount &&
        userAccount.role_id &&
        userAccount.role_id === 3 &&
        userCartList.length === 0 && (
          <div className="banner_after">
            <span>You have not had any scheduled tours yet!</span>
            <span>Scroll down to discover!</span>
          </div>
        )}

      {userAccount &&
        userAccount.role_id &&
        userAccount.role_id === 3 &&
        userCartList.length !== 0 && (
          <div className="banner_booked">
            <Slider {...settings}>
              {userCartList.map((item, key) => {
                return (
                  <div
                    key={key}
                    onClick={() => navigate(`/ticket/${item.cart_id}`)}
                    className="one_booked"
                  >
                    <span className="booked_name">{item.tour_name} </span>
                    <div className="booked_quan_date_price">
                      <div className="booked_quan">
                        <FontAwesomeIcon
                          icon={faUserGroup}
                          size="lg"
                          className="quan_icon"
                        />
                        <span>{item.number_tourists} </span>
                      </div>
                      <span className="booked_date">
                        {item.booked_time
                          ? item.booked_time.substring(0, 11)
                          : ""}{" "}
                      </span>
                      <span className="booked_price">${item.price} </span>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </div>
        )}

      {userAccount &&
        userAccount.role_id &&
        (userAccount.role_id === 1 || userAccount.role_id === 2) && (
          <div className="banner_name">
            <br />
            <span className="banner_name1" onClick={() => navigate("/post")}>
              Post Tour
            </span>{" "}
          </div>
        )}
      {(!userAccount || userAccount.role_id === undefined) && (
        <div className="banner_name">
          <span className="banner_name1">Ads tour of</span>
          <br />
          <span className="banner_name2">your</span>{" "}
          <span className="banner_name3" onClick={() => navigate("/registOrg")}>
            organization!
          </span>{" "}
        </div>
      )}
    </div>
  );
}

export default Banner;
