import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useNavigate } from "react-router-dom";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { responsive, scrollTop } from "../../../constants/global";
import { Images } from "../../../constants/images";
import StarRatings from "../../DetailPage/StarRating/star-rating";
import "./Suggestion.css";

function Suggestion(props) {
  const { suggestionList, imgDetails } = props;
  const navigate = useNavigate();

  return (
    <div className="suggestion">
      <span className="suggest_title">Suggestion for you</span>
      <div className="carousel_border">
        <Carousel responsive={responsive} className="carousel1">
          {suggestionList &&
            suggestionList.length > 0 &&
            suggestionList.map((item, key) => {
              return (
                <div
                  key={key}
                  className="carousel_item"
                  onClick={() => {
                    navigate(`/detail/${item.tour_id}/${item.org_id}`);
                    scrollTop();
                  }}
                >
                  <div className="carousel_item_body">
                    <img
                      className="carousel_item_img"
                      src={
                        `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                          ? `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                          : Images.THAILAN
                      }
                      alt=""
                    />

                    <div className="carousel_item_info">
                      <span className="carousel_item_info_name">
                        {" "}
                        {item.tour ? item.tour.tour_name : ""}
                      </span>
                      <div className="carousel_item_info_rate">
                        <StarRatings
                          svgIconViewBox="0 0 51 48"
                          starRatedColor="#fdd43c"
                          starEmptyColor="#fff"
                          starDimension="20px"
                          rating={item ? Math.round(item.avg_star) : 0}
                          starSpacing="5px"
                        />
                      </div>
                      <span className="carousel_item_info_price">
                        From {item.tour ? item.tour.price : 0}$
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
        </Carousel>
      </div>
    </div>
  );
}

export default Suggestion;
