import React, { useEffect, useState } from "react";
import Banner from "./Banner";
import Love from "./Love";
import "./Main.css";
import Search from "./Search";
import Suggestion from "./Suggestion";
import axios from "axios";
import Cookies from "universal-cookie";
import { getLoveData, getTopData, getUserCart } from "../../constants/api";

HomePage.propTypes = {};

function HomePage(props) {
  const cookies = new Cookies();
  const userToken = cookies.get("token");

  var [suggestionList, setSuggestionList] = useState([]);
  var [loveList, setLoveList] = useState([]);
  var [userCartList, setUserCartList] = useState([]);

  useEffect(() => {
    getTopData(suggestionList, { setSuggestionList });
    getLoveData(loveList, { setLoveList });
    getUserCart(userCartList, { setUserCartList });
  }, []);

  return (
    <div className="padding">
      <Banner userCartList={userCartList} />
      <Search />
      <Suggestion suggestionList={suggestionList} />
      <Love loveList={loveList} />
    </div>
  );
}

export default HomePage;
