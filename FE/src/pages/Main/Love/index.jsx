import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useNavigate } from "react-router-dom";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { responsive, scrollTop } from "../../../constants/global";
import { Images } from "../../../constants/images";
import "./Love.css";

Love.propTypes = {};

function Love(props) {
  const { loveList, imgDetails } = props;
  const navigate = useNavigate();

  return (
    <div className="love">
      <span className="love_title">
        Do you love <span>beaches</span> and <span>oceans</span> ?
      </span>
      <div className="love_carousel_border">
        <Carousel responsive={responsive} className="love_carousel1">
          {loveList &&
            loveList.length > 0 &&
            loveList.map((item, key) => {
              return (
                <div
                  key={key}
                  className="love_carousel_item"
                  onClick={() => {
                    navigate(`/detail/${item.tour_id}/${item.org_id}`);
                    scrollTop();
                  }}
                >
                  <div className="love_carousel_item_body">
                    <img
                      className="carousel_item_img"
                      src={
                        `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                          ? `http://localhost:5000/api/tours/images?tour=${item.tour_id}&org=${item.org_id}`
                          : Images.THAILAN
                      }
                      alt=""
                    />

                    {/* <img
                      src={Images.THAILAN}
                      className="love_carousel_item_img"
                      alt=""
                    /> */}
                    <div className="love_carousel_item_info">
                      <span className="love_carousel_item_info_name">
                        {" "}
                        {item.tour_name}
                      </span>
                      <div className="love_carousel_item_info_rate">
                        <FontAwesomeIcon icon={faCircle} size="sm" />
                        <FontAwesomeIcon icon={faCircle} size="sm" />
                        <FontAwesomeIcon icon={faCircle} size="sm" />
                        <FontAwesomeIcon icon={faCircle} size="sm" />
                        <FontAwesomeIcon icon={faCircle} size="sm" />
                      </div>
                      <span className="love_carousel_item_info_price">
                        From {item.price}$
                      </span>
                    </div>
                  </div>
                </div>
              );
            })}
        </Carousel>
      </div>
    </div>
  );
}

export default Love;
