import React from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import HomePage from "./Main";

Tour.propTypes = {};

function Tour(props) {
  const { pathname } = useLocation();
  console.log("🚀 ~ file: index.jsx ~ line 10 ~ Tour ~ pathname", pathname);
  return (
    <Routes>
      <Route exact path={pathname} element={<HomePage />} />
    </Routes>
  );
}

export default Tour;
