import axios from "axios";
import React, { useState, useEffect } from "react";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import "../PostPage.css";

export const LocationOptions = ({ handleChange }) => {
  const animatedComponents = makeAnimated();

  const [locationList, setLocationList] = useState([]);
  const [locationOptions, setLocationOptions] = useState("");

  useEffect(() => {
    getLocation();
  }, []);

  const locationOption = locationList.map((item) => ({
    label: item.location_name,
    value: item.country_id,
  }));

  const getLocation = async () => {
    try {
      const res = await axios.get(
        "http://localhost:5000/api/countries/locations"
      );
      console.log("🚀 ~ file: API.js ~ line 186 ~ getLocation ~ res", res.data);
      setLocationList(res.data.message);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div>
      <Select
        name="Location_name"
        placeholder="Select location"
        // isDisabled={isDisabled}
        components={animatedComponents}
        // onChange={(e) => {
        //   setLocationOptions(e.label);
        // }}
        onChange={(selectedOption) => {
          handleChange("Location_name")(selectedOption.label);
        }}
        className="locationOptions_container countries react-select-container"
        classNamePrefix="react-select"
        options={locationOption}
        isSearchable
      />
    </div>
  );
};
