import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Fade,
  Modal,
  Typography,
} from "@mui/material";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import Notification from "../../../components/Notification";
import { handleCreateLocation } from "../../../constants/api";
import { style } from "../../../constants/global";
import {
  createLocationSchema,
  initialCountryOptionValues,
} from "../../../constants/validate";
import "../PostPage.css";

export const LocationCreated = () => {
  const animatedComponents = makeAnimated();
  const countriesStorage = useSelector(
    (state) => state.countriesList.countriesList
  );

  const [countriesList, setCountriesList] = useState([]);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  useEffect(() => {
    setCountriesList(countriesStorage);
  }, [countriesStorage, countriesList]);

  const countriesOption = countriesList.map((item) => ({
    label: `${item.country_code} - ${item.country_name}`,
    value: item.country_code,
  }));

  return (
    <div>
      <Button variant="outlined" onClick={handleOpen} className="button_modal">
        +
      </Button>
      <Modal
        className="modal"
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Formik
              initialValues={initialCountryOptionValues}
              validationSchema={createLocationSchema}
              onSubmit={(values, { setSubmitting }) => {
                handleCreateLocation(values, { setSubmitting }, { setNotify });
              }}
            >
              {(formikProps) => {
                const { isSubmitting, handleChange } = formikProps;
                return (
                  <Form>
                    <Typography
                      id="transition-modal-title"
                      variant="h6"
                      component="h2"
                    >
                      Please enter the location
                    </Typography>
                    <Field
                      name="Location_name"
                      type="text"
                      className="input"
                      placeholder="Enter the location of this trip"
                      spellCheck="false"
                    />
                    <ErrorMessage name="Location_name" />

                    <div className="countriesOptions_container">
                      <Select
                        name="Country"
                        placeholder="Select countries"
                        // isDisabled={isDisabled}
                        // value={countriesOptions}
                        components={animatedComponents}
                        onChange={(selectedOption) => {
                          handleChange("Country")(selectedOption.value);
                        }}
                        className="select input_country react-select-container"
                        classNamePrefix="react-select"
                        options={countriesOption}
                        isSearchable
                      />
                      <ErrorMessage name="Country" />
                    </div>

                    <Button type="submit" className="create" variant="outlined">
                      {isSubmitting ? (
                        <CircularProgress size={25} color="success" />
                      ) : (
                        "Create"
                      )}
                    </Button>
                  </Form>
                );
              }}
            </Formik>
            <Notification notify={notify} />
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};
