import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, CircularProgress } from "@mui/material";
import axios from "axios";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState } from "react";
import Cookies from "universal-cookie";
import Notification from "../../components/Notification";
import {
  createTourValidationSchema,
  initialTourValues,
} from "../../constants/validate";
import { LocationCreated } from "./CreateLocation";
import { LocationOptions } from "./LocationOptions";
import "./PostPage.css";

function PostPage() {
  const cookies = new Cookies();
  const userToken = cookies.get("token");

  const [image, setImage] = useState(null);
  const [selectedFile, setSelectedFile] = useState(null);
  var [tourInfo, setTourInfo] = useState(null);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  const onImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      setImage(URL.createObjectURL(e.target.files[0]));
      setSelectedFile(e.target.files[0]);
    }
  };

  const handleCreateTour = async (values, { setSubmitting }) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    };
    const body = {
      location_name: values.Location_name,
      tour_name: values.Tour_name,
      price: values.Price,
      number_days: Number(values.Days),
      number_nights: Number(values.Nights),
      introduce: values.Introduce,
      description: values.Description,
    };
    console.log(
      "🚀 ~ file: index.jsx ~ line 46 ~ handleCreateTour ~ body",
      body
    );

    try {
      const res = await axios.post(
        `http://localhost:5000/api/tours/create`,
        body,
        config
      );
      console.log("🚀 ~ file: API.js ~ line 254 ~ handleCreateTour ~ res", res);
      setTimeout(() => {
        console.log("value : ", values);
        setSubmitting(false);
      }, 300);

      tourInfo = res.data.message;
      setTourInfo(tourInfo);
    } catch (error) {
      if (error.response.status === 400)
        setNotify({
          message: error.response.data.message,
          type: "error",
        });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleUploadImg = async () => {
    if (!tourInfo) return;
    const formData = await new FormData();

    // Update the formData object
    await formData.append("files", selectedFile);
    await formData.append(
      "name",
      `tour-${tourInfo.tour_id}-org-${tourInfo.org_id}`
    );

    try {
      const res = await axios.post(
        `http://localhost:5000/api/tours/images`,
        formData
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 38 ~ handleUploadImg ~ res",
        res.data
      );
      setTimeout(() => {
        setNotify({
          message: "Successful!!",
          type: "success",
        });
      }, 1000);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleSubmitTour = async (values, { setSubmitting }) => {
    await handleCreateTour(values, { setSubmitting });
    await handleUploadImg();
  };

  return (
    <div className="postpage_container">
      <Formik
        validationSchema={createTourValidationSchema}
        initialValues={initialTourValues}
        onSubmit={(values, { setSubmitting }) => {
          handleSubmitTour(values, { setSubmitting });
        }}
      >
        {(formikProps) => {
          const { isSubmitting, handleChange } = formikProps;
          return (
            <Form className="post_side">
              <div className="post_title">
                {image ? (
                  <img className="preview_img" src={image} alt="preview" />
                ) : (
                  <label className="img_picker">
                    <FontAwesomeIcon
                      className="icon"
                      icon={faCamera}
                      size="3x"
                      style={{ color: "#464646" }}
                    />
                    <input type="file" onChange={onImageChange} required />
                  </label>
                )}
                <div className="post_name_price">
                  <div className="post_name">
                    <Field
                      name="Tour_name"
                      className="input"
                      type="text"
                      placeholder="Please type your tour's name"
                      spellCheck="false"
                    />
                    <ErrorMessage name="Tour_name" />
                  </div>
                  <div className="post_price">
                    <Field
                      name="Price"
                      className="input"
                      type="text"
                      placeholder="Please type your tour's price"
                      spellCheck="false"
                    />
                    <ErrorMessage name="Price" />
                  </div>
                  <div className="post_time">
                    <div className="post_time_day">
                      <Field
                        name="Days"
                        className="input"
                        type="text"
                        placeholder=" Days "
                        spellCheck="false"
                      />
                      <ErrorMessage name="Days" />
                    </div>

                    <div className="post_time_night">
                      <Field
                        name="Nights"
                        className="input"
                        type="text"
                        placeholder=" Nights "
                        spellCheck="false"
                      />
                      <ErrorMessage name="Nights" />
                    </div>
                  </div>

                  <div className="post_location">
                    <LocationOptions handleChange={handleChange} />
                    <LocationCreated />
                  </div>
                </div>
              </div>

              <div className="post_body">
                <div className="post_introduce describe">
                  <h2>Please write an introduce for this trip</h2>
                  <Field className="textarea" name="Introduce" />
                  <ErrorMessage name="Introduce" />
                </div>
                <div className="post_description describe">
                  <h2>Please write a description of this trip</h2>
                  <Field className="textarea" name="Description" />
                  <ErrorMessage name="Description" />
                </div>
                <Button type="submit" variant="outlined">
                  {isSubmitting ? (
                    <CircularProgress size={21} color="success" />
                  ) : (
                    "Create a tour"
                  )}
                </Button>
                <Notification notify={notify} />
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default PostPage;
