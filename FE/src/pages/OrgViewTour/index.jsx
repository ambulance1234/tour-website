import axios from "axios";
import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import "./OrgViewTour.css";
import { useNavigate } from "react-router-dom";

OrgViewTour.propTypes = {};

function OrgViewTour(props) {
  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const userAccount = cookies.get("account");
  const navigate = useNavigate();

  var [listTour, setListTour] = useState([]);
  var [categories, setCategories] = useState([]);

  useEffect(() => {
    getListTour();
  }, []);

  const getListTour = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/org?org=${userAccount.account_id}`,
        config
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 24 ~ getListTour ~ res",
        res.data.message
      );

      listTour = res.data.message;
      setListTour(listTour);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleDeleteTour = async () => {
    try {
      const res = await axios.delete(`http://localhost:5000/api/tours/delete`, {
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
        data: {
          tours: categories,
        },
      });
      // alert()
      console.log(
        "🚀 ~ file: index.jsx ~ line 65 ~ handleDeleteTour ~ res",
        res
      );
      navigate(0);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleMultiple = (id) => {
    console.log("clicked");
    var index = categories.findIndex((item) => item && item === id);
    if (index < 0) {
      categories.push(id);
    } else {
      categories.splice(index, 1);
    }
    console.log(
      "🚀 ~ file: index.jsx ~ line 71 ~ handleMultiple ~ categories",
      categories
    );
    setCategories(categories);
  };

  return (
    <div className="org_view_tour">
      <div className="org_view_tour_layout">
        <div className="org_view_tour_table">
          <table>
            <tr>
              <th>
                <input type="checkbox"></input>
              </th>
              <th>ID</th>
              <th>Name</th>
              <th>Location</th>
              <th>Edit</th>
            </tr>
            {listTour &&
              listTour.length > 0 &&
              listTour.map((item, key) => {
                return (
                  <tr key={key}>
                    <td>
                      <input
                        onClick={() => handleMultiple(item.tour_id)}
                        type="checkbox"
                      />
                    </td>
                    <td>{item.tour_id} </td>
                    <td
                      onClick={() =>
                        navigate(`/orgViewDetailTour/${item.tour_id}`)
                      }
                    >
                      {item.tour_name}{" "}
                    </td>
                    <td>{item.location.location_name}</td>
                    <td>
                      <button
                        onClick={() => navigate(`/edit-post/${item.tour_id}`)}
                        className="org_view_tour_table_edit"
                      >
                        Edit
                      </button>
                    </td>
                  </tr>
                );
              })}
          </table>
        </div>
        <div className="org_view_tour_del">
          <button onClick={() => navigate(`/post`)}>Add Tour</button>
          <button onClick={handleDeleteTour}>Delete</button>
        </div>
      </div>

      {/* Bấm edit hiện modal */}
      {/* <div className="org_view_tour_modal">
                <div className="modal_edit_form">
                    <p className="edit_form_title">Edit Tour</p>
                    <div className="edit_form_input">
                        <span>Name</span>
                        <input/>
                        <span>Location</span>
                        <input/>
                    </div>
                    <div className="edit_form_btns">
                        <button className="edit_form_btns_cancle">Cancle</button>
                        <button className="edit_form_btns_accept">Accept</button>
                    </div>
                </div>
            </div> */}
    </div>
  );
}

export default OrgViewTour;
