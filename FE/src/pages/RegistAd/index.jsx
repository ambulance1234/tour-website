import React from "react";
import { Images } from "../../constants/images";
import "./RegistAd.css";

RegistAd.propTypes = {};

function RegistAd(props) {
  return (
    <div className="regist_user">
      <img src={Images.VINHHALONG} className="bg_img" alt="" />
      <div className="modal">
        <div className="logo">
          <div className="logo_img_border">
            <img src={Images.LOGO} alt="" />
          </div>
          <div className="logo_name">
            <span>Tour</span>
            <span>Ads</span>
          </div>
        </div>
        <div className="input_box">
          <div className="input_box_up">
            <div className="input_box1">
              <input
                className="input_bar"
                type="text"
                placeholder="Email"
              ></input>
              <input
                className="input_bar"
                type="text"
                placeholder="Phone"
              ></input>
              <input
                className="input_bar"
                type="password"
                placeholder="Password"
              ></input>
              <input
                className="input_bar"
                type="password"
                placeholder="Key"
              ></input>
            </div>
            <div className="input_box2">
              <input
                className="input_bar"
                type="text"
                placeholder="User Name"
              ></input>
              <div className="input_bar input_bar_country">
                <select className="input_country">
                  <option value="none" selected disabled hidden>
                    Select country
                  </option>
                  <option>home </option>
                  <option>market</option>
                  <option>super market</option>
                  <option>Italy market</option>
                </select>
              </div>
              <input
                className="input_bar"
                type="password"
                placeholder="Confirm password"
              ></input>
              <input
                className="input_bar hidd"
                disabled
                type="password"
                placeholder="Confirm code (Custom)"
              ></input>
            </div>
          </div>
        </div>
        <div className="agree">
          <label class="container">
            I agree with the license
            <input type="checkbox" />
            <span className="checkmark"></span>
          </label>
        </div>
        <button className="signUp_btn">Sign up</button>
      </div>
    </div>
  );
}
export default RegistAd;
