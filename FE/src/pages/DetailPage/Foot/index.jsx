import React from "react";
import { Images } from "../../../constants/images";
import { Evaluate } from "../Evaluate";
import "./Reviews.css";

export const Foot = ({ tourDetail }) => {
  return (
    <div className="review_container">
      <div className="covid_note">
        <div className="covid_note_title">
          <div className="title_img">
            <img src={Images.HANDWASH} alt="" />
          </div>
          <h2>Safe travel during the COVID-19 pandemic</h2>
        </div>
        <div className="covid_note_info">
          <h2>What you can expect in this experience</h2>
          <div className="info">
            <ul className="info_1 row">
              <li>Tour guides must often wash their hands </li>
              <li>Tourists must wear masks in public areas</li>
              <li>Tour guides must wear masks in public areas</li>
              <li>Tools/equipment are disinfected between uses</li>
              <li>Hand sanitizer available for tourists and staff</li>
            </ul>
            <ul className="info_2 row">
              <li>Regular temperature checks for employees</li>
              <li>Temperature checks for tourists upon arrival</li>
              <li>Vehicles are disinfected regularly</li>
              <li>Tourists are provided with masks</li>
              <li>Pay-at-home policy for employees with symptoms</li>
            </ul>
          </div>
        </div>
      </div>

      <div className="introduction">
        <div className="introduce intro">
          <h2>Introduce</h2>
          <p>
            {tourDetail && tourDetail.tours ? tourDetail.tours.introduce : ""}
          </p>
        </div>
        <div className="description intro">
          <h2>Description</h2>
          <p>
            {tourDetail && tourDetail.tours ? tourDetail.tours.description : ""}
          </p>
        </div>
      </div>

      <div className="evaluate">
        <Evaluate tourDetail={tourDetail} />
      </div>
    </div>
  );
};
