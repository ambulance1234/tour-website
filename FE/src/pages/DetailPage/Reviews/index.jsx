import { faThumbsUp } from "@fortawesome/free-regular-svg-icons";
import { faThumbsDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Images } from "../../../constants/images";
import StarRatings from "../StarRating/star-rating";
import "../Evaluate/Comment.css";
import { useSelector } from "react-redux";
import Cookies from "universal-cookie";
import axios from "axios";
import moment from "moment";

export const Reviews = ({ openCmt }) => {
  const cookies = new Cookies();

  const tourId = localStorage.getItem("tourId");
  const orgId = localStorage.getItem("orgId");
  const userToken = cookies.get("token");
  const userAccount = cookies.get("account");

  const reviewsStorage = useSelector((state) => state.detail.reviewsData);

  const [like, setLike] = useState(false);

  const upvote = () => {
    setLike(!like);
    handleUpvote();
  };

  const handleUpvote = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const body = {
      user_id: userAccount.account_id,
      tour_id: tourId,
      org_id: orgId,
    };
    console.log("🚀 ~ file: index.jsx ~ line 41 ~ handleUpvote ~ body", body);

    try {
      const res = await axios.put(
        "http://localhost:5000/api/tours/reviews/create",
        body,
        config
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 29 ~ handleComment ~ res",
        res.data
      );
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div>
      {reviewsStorage &&
        reviewsStorage.rows.map((item, key) => {
          return (
            <div
              key={key}
              className={openCmt ? "expanded_comment" : "review_comment"}
            >
              <div className="comment_user">
                <div className="user_avt">
                  <img src={Images.JOKER} alt="" />
                </div>
                <div className="user_detail">
                  <b>{item && item.user ? item.user.username : ""} </b>&nbsp;{" "}
                  <p>
                    wrote a review in{" "}
                    {item && item.created_at
                      ? moment(item.created_at).startOf().fromNow()
                      : 0}{" "}
                  </p>
                </div>
                <div className="user_like">
                  {like ? (
                    <FontAwesomeIcon
                      onClick={upvote}
                      className="like_icon"
                      icon={faThumbsDown}
                    />
                  ) : (
                    <FontAwesomeIcon
                      onClick={upvote}
                      className="like_icon"
                      icon={faThumbsUp}
                    />
                  )}
                  <p>{item ? item.upvote : ""} people liked this review</p>
                </div>
              </div>

              <div className="comment_detail">
                <StarRatings
                  svgIconViewBox="0 0 51 48"
                  starRatedColor="#fdd43c"
                  starEmptyColor="#fff"
                  starDimension="30px"
                  rating={item ? item.number_stars : 0}
                  starSpacing="5px"
                />

                <p>{item ? item.comment : ""}</p>
              </div>
            </div>
          );
        })}
    </div>
  );
};
