import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState, useEffect } from "react";
import { Images } from "../../../constants/images";
import Rate from "../Rate";
import styles from "./Comment.module.css";
import Fade from "react-reveal/Fade";
import axios from "axios";
import Cookies from "universal-cookie";
import Notification from "../../../components/Notification";

export const Comment = ({ openCmt, setOpenCmt }) => {
  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const tourId = localStorage.getItem("tourId");
  const orgId = localStorage.getItem("orgId");

  var [isRated, setIsRated] = useState(false);
  const [cmtStar, setCmtStar] = useState(0);
  const [comment, setComment] = useState("");
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  useEffect(() => {
    console.log("🚀 ~ file: index.jsx ~ line 13 ~ Comment ~ cmtStar", cmtStar);
  }, [cmtStar]);

  const handleComment = async (e) => {
    e.preventDefault();

    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };

    const body = {
      tour_id: tourId,
      org_id: orgId,
      number_stars: cmtStar,
      comment: comment,
    };

    try {
      const res = await axios.post(
        "http://localhost:5000/api/tours/reviews/create",
        body,
        config
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 29 ~ handleComment ~ res",
        res.data
      );
      setNotify({
        message: res.statusText,
        type: "success",
      });
      setOpenCmt(false);
    } catch (error) {
      setNotify({
        message: error.response.data.message,
        type: "error",
      });
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div>
      <Fade opposite collapse when={openCmt}>
        <form id="cmt_form" onSubmit={handleComment} className={styles.my_cmt}>
          <div className={styles.avatar}>
            <img src={Images.EMPTY} alt="avatar" />
          </div>
          <span className={styles.name}>hahalolo</span>
          <div className={styles.star}>
            <Rate
              rating={cmtStar}
              onRating={(rate) => {
                isRated = !isRated;
                setIsRated(isRated);
                setCmtStar(isRated ? rate : 0);
              }}
            />
          </div>
          <div className={styles.text_area}>
            <textarea
              placeholder="Write your comment here..."
              name="your_comment"
              id="my_cmt"
              cols="50"
              rows="10"
              spellCheck="false"
              required
              onChange={(e) => setComment(e.target.value)}
              // disabled={
              //   latestComment && latestComment.comment && !isRated
              //     ? isDisabled
              //     : ""
              // }
            ></textarea>
          </div>
          <span className={styles.warn}>
            Your review will be on public.You can rate star only!
            <FontAwesomeIcon
              className={styles.icon}
              size="1x"
              icon={faInfoCircle}
              style={{ color: "#000" }}
            />
          </span>
          <div className={styles.submit}>
            <button
              type="button"
              // onClick={() => setOpenComment(false)}
              className={styles.cancel}
            >
              Cancel
            </button>

            <button className={styles.send} type="submit">
              Send
            </button>
          </div>
        </form>
        <Notification notify={notify} />
      </Fade>
    </div>
  );
};
