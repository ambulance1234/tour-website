import { faComment } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@mui/material";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Comment } from "../Comment";
import { Reviews } from "../Reviews";
import "./Comment.css";

export const Evaluate = () => {
  const reviewsStorage = useSelector((state) => state.detail.reviewsData);
  const [openCmt, setOpenCmt] = useState(false);

  return (
    <div className="evaluate_container">
      <div className="review_count">
        <FontAwesomeIcon icon={faComment} size="2x" />
        <p>{reviewsStorage ? reviewsStorage.count : 0} </p>
        <p>Reviews</p>
      </div>
      <div className="review_title">
        <h2>Reviews</h2>
        <Button onClick={() => setOpenCmt(!openCmt)} variant="outlined">
          Review this place
        </Button>
      </div>
      <Comment openCmt={openCmt} setOpenCmt={setOpenCmt} />
      <Reviews openCmt={openCmt} />
    </div>
  );
};
