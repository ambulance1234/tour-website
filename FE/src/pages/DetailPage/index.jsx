import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { reviewData } from "../../actions/Detail";
import { getTourDetail } from "../../constants/api";
import { Body } from "./Body";
import "./Detail.css";
import { Foot } from "./Foot";
import { Head } from "./Head";

Detail.propTypes = {};

function Detail(props) {
  const { tourId, orgId } = useParams();

  const dispatch = useDispatch();

  var [tourDetail, setTourDetail] = useState(null);
  var [reviewsData, setReviewsData] = useState(null);
  var [imgDetails, setImgDetails] = useState("");

  useEffect(() => {
    getTourDetail(tourDetail, { setTourDetail });
    getTourReviews();
    getImageDetail();
  }, []);

  useEffect(() => {
    localStorage.setItem("tourId", tourId);
    localStorage.setItem("orgId", orgId);
  }, [tourId, orgId]);

  const getTourReviews = async () => {
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/reviews?tour=${tourId}&org=${orgId}&page=1`
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 50 ~ getTourReviews ~ res",
        res.data.message
      );

      reviewsData = res.data.message;
      setReviewsData(reviewsData);

      if (reviewsData) dispatch(reviewData(reviewsData));
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const getImageDetail = async () => {
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/images?tour=${tourId}&org=${orgId}`
      );
      console.log("🚀 ~ file: index.jsx ~ line 77 ~ getImageDetail ~ res", res);
      imgDetails = `http://localhost:5000/api/tours/images?tour=${tourId}&org=${orgId}`;
      setImgDetails(imgDetails);
    } catch (error) {
      if (error && error.response === 404) return setImgDetails("");
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div className="detail_main">
      <Head tourDetail={tourDetail} />
      <Body imgDetails={imgDetails} tourDetail={tourDetail} />
      <Foot tourDetail={tourDetail} />
    </div>
  );
}

export default Detail;
