import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import AddBoxOutlinedIcon from "@mui/icons-material/AddBoxOutlined";
import RemoveOutlinedIcon from "@mui/icons-material/RemoveOutlined";
import { DatePicker, LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { CircularProgress, TextField } from "@mui/material";
import { Form, Formik } from "formik";
import { motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { decrease, increase } from "../../../actions/Count";
import Notification from "../../../components/Notification";
import { handleBookTour, HandleBookTour } from "../../../constants/api";
import { Images } from "../../../constants/images";
import {
  bookTourSchema,
  initialBookTourValues,
} from "../../../constants/validate";
import "./Body.css";

export const Body = ({ tourDetail, imgDetails }) => {
  const dispatch = useDispatch();

  const [value, setValue] = useState(null);
  const [numberOfPeople, setNumberOfPeople] = useState(1);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  useEffect(() => {
    localStorage.setItem("count", numberOfPeople);
  }, [numberOfPeople]);

  const handleIncrease = () => {
    // dispatch(increase(numberOfPeople));
    setNumberOfPeople(numberOfPeople + 1);
  };

  const handleDecrease = () => {
    if (numberOfPeople === 1) return;
    // dispatch(decrease(numberOfPeople));
    setNumberOfPeople(numberOfPeople - 1);
  };

  return (
    <div className="detail_booking">
      <div className="booking">
        <div className="booking_img">
          <img src={imgDetails ? imgDetails : Images.LONDON} alt="" />
          {/* <img src={Images.LONDON} alt="" /> */}
        </div>
        <div className="booking_detail">
          <Formik
            validationSchema={bookTourSchema}
            initialValues={initialBookTourValues}
            onSubmit={(values, { setSubmitting }) => {
              handleBookTour(values, { setSubmitting }, numberOfPeople, {
                setNotify,
              });
            }}
          >
            {(formikProps) => {
              const { isSubmitting, setFieldValue, values } = formikProps;
              return (
                <Form>
                  <div className="date">
                    <div className="price">
                      <h2>Select dates and tourists</h2>
                      <h2>
                        {" "}
                        {tourDetail && tourDetail.tours
                          ? tourDetail.tours.price
                          : ""}
                        $
                      </h2>
                    </div>
                    <div className="option">
                      <div className="date">
                        <motion.div
                          whileHover={{ scale: 1.1 }}
                          whileTap={{ scale: 1 }}
                        >
                          <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                              name="Booked_time"
                              label="Please pick a time"
                              value={value}
                              onChange={(newValue) => {
                                setValue(newValue);
                                setFieldValue("Booked_time", newValue);
                              }}
                              renderInput={(params) => (
                                <TextField size="small" {...params} />
                              )}
                            />
                          </LocalizationProvider>
                        </motion.div>
                      </div>
                      <motion.div
                        whileHover={{ scale: 1.1 }}
                        whileTap={{ scale: 1 }}
                      >
                        <div className="peopleCount opt">
                          <div onClick={handleDecrease}>
                            <RemoveOutlinedIcon />
                          </div>
                          <div className="count">
                            <FontAwesomeIcon
                              className="user_icon"
                              icon={faUser}
                            />
                            <p>{numberOfPeople} people</p>
                          </div>
                          <div onClick={handleIncrease}>
                            <AddBoxOutlinedIcon />
                          </div>
                        </div>
                      </motion.div>
                    </div>
                    <div className="another_option">
                      <h2>
                        Trip time :{" "}
                        {tourDetail && tourDetail.tours
                          ? tourDetail.tours.number_days
                          : ""}{" "}
                        days{" "}
                        {tourDetail && tourDetail.tours
                          ? tourDetail.tours.number_nights
                          : ""}{" "}
                        nights
                      </h2>
                      <button type="submit">
                        {isSubmitting ? (
                          <CircularProgress size={21} color="success" />
                        ) : (
                          "Book tour"
                        )}
                      </button>
                      <Notification notify={notify} />
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};
