import React from "react";
import StarRatings from "../StarRating/star-rating";
import "./Head.css";

export const Head = ({ tourDetail }) => {
  console.log("🚀 ~ file: index.jsx ~ line 5 ~ Head ~ tourDetail", tourDetail);
  return (
    <div className="content_title">
      <div className="content_title_head">
        <div className="content">
          <h2 className="name">
            {`${
              tourDetail && tourDetail.tours ? tourDetail.tours.tour_name : ""
            } - ${
              tourDetail && tourDetail.tours
                ? tourDetail.tours.location.location_name
                : ""
            }`}
          </h2>
          {/* <h2>
            {tourDetail && tourDetail.tours ? tourDetail.tours.org.address : ""}{" "}
          </h2> */}
          <p className="by">
            By :{" "}
            {tourDetail && tourDetail.tours
              ? tourDetail.tours.org.org_name
              : ""}{" "}
          </p>
          <div className="rate">
            <StarRatings
              svgIconViewBox="0 0 51 48"
              starRatedColor="#fdd43c"
              starEmptyColor="#fff"
              starDimension="20px"
              rating={
                tourDetail && tourDetail.preliminary
                  ? Math.round(tourDetail.preliminary.average_star)
                  : 0
              }
              starSpacing="5px"
            />
            <span className="rate_numb">
              {tourDetail ? tourDetail.preliminary.total_reviews : ""} people
            </span>
          </div>
        </div>

        <div className="covid">
          <div className="covid_content">
            <p>
              <b>COVID-19 Update:</b> See additional health and safety measures
              this experience is taking
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
