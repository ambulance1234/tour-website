import { CircularProgress } from "@mui/material";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import Notification from "../../components/Notification";
import { getCountries, handleRegisterUserSubmit } from "../../constants/api";
import { Images } from "../../constants/images";
import {
  initialRegistUserValues,
  registUserValidationSchema,
} from "../../constants/validate";
import "./RegistUser.css";
RegistUser.propTypes = {};

function RegistUser(props) {
  const navigate = useNavigate();
  const animatedComponents = makeAnimated();

  const [countriesList, setCountriesList] = useState([]);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  useEffect(() => {
    getCountries(countriesList, { setCountriesList });
  }, []);

  const countriesOption = countriesList.map((item) => ({
    label: `${item.country_code} - ${item.country_name}`,
    value: item.country_code,
  }));

  return (
    <div className="regist_user">
      <img alt="" src={Images.EIFFEL} className="bg_img" />
      <Formik
        validationSchema={registUserValidationSchema}
        initialValues={initialRegistUserValues}
        onSubmit={(values, { setSubmitting }) => {
          handleRegisterUserSubmit(values, { setSubmitting }, { setNotify });
        }}
      >
        {(formikProps) => {
          const { isSubmitting, handleChange } = formikProps;
          return (
            <Form className="modal">
              <div className="logo">
                <div className="logo_img_border">
                  <img alt="" src={Images.LOGO} />
                </div>
                <div className="logo_name" onClick={() => navigate("/home")}>
                  <span>Tour</span>
                  <span>Ads</span>
                </div>
              </div>
              <div className="input_box">
                <div className="input_box1">
                  <Field
                    name="Email"
                    className="input_bar"
                    type="email"
                    placeholder="Email"
                  />
                  <ErrorMessage name="Email" />

                  <Field
                    name="Password"
                    className="input_bar"
                    type="password"
                    placeholder="Password"
                  />
                  <ErrorMessage name="Password" />

                  <Field
                    name="Mobile"
                    className="input_bar"
                    type="text"
                    placeholder="Phone"
                  />
                  <ErrorMessage name="Mobile" />
                </div>
                <div className="input_box2">
                  <Field
                    name="Username"
                    className="input_bar"
                    type="text"
                    placeholder="User Name"
                  />
                  <ErrorMessage name="Username" />

                  <Field
                    name="ConfirmPassword"
                    className="input_bar"
                    type="password"
                    placeholder="Confirm password"
                  />
                  <ErrorMessage name="ConfirmPassword" />
                  <div className="input_bar input_bar_country">
                    <Select
                      name="Country"
                      placeholder="Select country"
                      // isDisabled={isDisabled}
                      components={animatedComponents}
                      onChange={(selectedOption) => {
                        handleChange("Country")(selectedOption.value);
                      }}
                      className="select input_country react-select-container"
                      classNamePrefix="react-select"
                      options={countriesOption}
                      isSearchable
                    />
                    <ErrorMessage name="Country" />
                  </div>
                </div>
              </div>
              <div className="agree">
                <label class="container">
                  I agree with the license
                  <Field name="IsAgree" type="checkbox" />
                  <span className="checkmark"></span>
                </label>
              </div>
              <ErrorMessage name="IsAgree" />
              <button type="submit" className="signUp_btn">
                {isSubmitting ? (
                  <CircularProgress color="success" />
                ) : (
                  "Sign Up"
                )}
              </button>
              <Notification notify={notify} />
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default RegistUser;
