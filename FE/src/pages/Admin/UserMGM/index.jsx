import React from "react";
import PropTypes from "prop-types";
import "./UserMGM.css";
import { Images } from "../../../constants/images";

UserMGM.propTypes = {};

function UserMGM(props) {
  const { userAccountList } = props;
  return (
    <div className="user_mgm">
      <div className="user_mgm_table">
        <table>
          <tr>
            <th>
              <input type="checkbox"></input>
            </th>
            <th>ID</th>
            <th>Full Name</th>
            <th>Age</th>
            <th>User Name</th>
            <th>Gender</th>
            <th>Adresss</th>
          </tr>
          {userAccountList && userAccountList.length === 0 && (
            <div className="detail_tour_null">
              <img src={Images.EMPTY} alt="" />
              No one have booked this tour yet!!🤔
            </div>
          )}

          {userAccountList &&
            userAccountList.length !== 0 &&
            userAccountList.map((item, key) => {
              return (
                <tr key={key}>
                  <td>
                    <input type="checkbox"></input>
                  </td>
                  <td>{item.account_id} </td>
                  <td>{item.fullname} </td>
                  <td>{item.age} </td>
                  <td>{item.username} </td>
                  <td>{item.gender} </td>
                  <td>{item.address}</td>
                </tr>
              );
            })}
          {/* <tr>
                        <td><input type="checkbox"></input></td>
                        <td>1</td>
                        <td>nguyenvantuan23032000hiie@gmail.com</td>
                        <td>0123456789</td>
                        <td>vantuan01</td>
                        <td>Gay</td>
                        <td>1999/69, hẽm 48, đường Bùi Thị Xuân, quận Tân Bình, TP.Hồ Chí Minh</td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"></input></td>
                        <td>1</td>
                        <td>nguyenvantuan23032000hiie@gmail.com</td>
                        <td>0123456789</td>
                        <td>vantuan01</td>
                        <td>Gay</td>
                        <td>1999/69, hẽm 48, đường Bùi Thị Xuân, quận Tân Bình, TP.Hồ Chí Minh</td>
                    </tr> */}
        </table>
      </div>
      {/* <div className="user_mgm_del">
        <button>Delete</button>
      </div> */}
    </div>
  );
}

export default UserMGM;
