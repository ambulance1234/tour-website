import React, { useEffect, useState } from "react";
import { getOrgAccount, getUserAccount } from "../../constants/api";
import "./Admin.css";
import UserMGM from "./UserMGM";

export const AdminPage = () => {
  var [userAccountList, setUserAccountList] = useState([]);
  var [orgAccountList, setOrgAccountList] = useState([]);

  useEffect(() => {
    getUserAccount(userAccountList, { setUserAccountList });
    getOrgAccount(orgAccountList, { setOrgAccountList });
  }, []);

  return (
    <div className="admin_page">
      <div className="admin_sidebar">
        <div className="sidebar_container">
          <span className="siderbar_option">Account of</span>
          <button className="siderbar_option">User</button>
          <button className="siderbar_option">Organization</button>
          <button className="siderbar_option">Admin</button>
        </div>
      </div>

      <div className="admin_tables">
        <UserMGM userAccountList={userAccountList} />
      </div>
    </div>
  );
};
