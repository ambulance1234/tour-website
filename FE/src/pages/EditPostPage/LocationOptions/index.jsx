import axios from "axios";
import React, { useState, useEffect } from "react";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import { getLocation } from "../../../constants/api";
import "../Edit.css";

export const LocationOptions = ({ tourDetail, handleChange }) => {
  const animatedComponents = makeAnimated();

  const [locationList, setLocationList] = useState([]);
  const [locationOptions, setLocationOptions] = useState("");

  useEffect(() => {
    getLocation(locationList, { setLocationList });
  }, []);

  const locationOption = locationList.map((item) => ({
    label: item.location_name,
    value: item.country_id,
  }));

  return (
    <div>
      <Select
        name="Location_name"
        placeholder={
          tourDetail && tourDetail.location
            ? tourDetail.location.location_name
            : "Please choose your tour's location"
        }
        // isDisabled={isDisabled}
        components={animatedComponents}
        onChange={(selectedOption) => {
          handleChange("Location_name")(selectedOption.label);
        }}
        className="locationOptions_container countries react-select-container"
        classNamePrefix="react-select"
        options={locationOption}
        isSearchable
      />
    </div>
  );
};
