import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, CircularProgress } from "@mui/material";
import axios from "axios";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import Cookies from "universal-cookie";
import Notification from "../../components/Notification";
import { handleCreateTour } from "../../constants/api";
import { Images } from "../../constants/images";
import {
  createTourValidationSchema,
  initialTourValues,
} from "../../constants/validate";
import { LocationCreated } from "./CreateLocation";
import "./Edit.css";
import { LocationOptions } from "./LocationOptions";

function EditPostPage(props) {
  const { tourId } = useParams();
  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const userAccount = cookies.get("account");

  var [tourDetail, setTourDetail] = useState(null);
  const [image, setImage] = useState(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [tourInfo, setTourInfo] = useState(null);
  const [notify, setNotify] = useState({
    message: "",
    type: "",
  });

  useEffect(() => {
    getTourDetail();
  }, []);

  const getTourDetail = async () => {
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/detail?tour=${tourId}&org=${userAccount.account_id}`
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 30 ~ getTourDetail ~ res",
        res.data.message
      );
      tourDetail = res.data.message.tours;
      setTourDetail(tourDetail);
      console.log(
        "🚀 ~ file: index.jsx ~ line 47 ~ getTourDetail ~ tourDetail",
        tourDetail
      );
    } catch (error) {
      if (error && error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
    }
  };

  const handleCreateTour = async (values, { setSubmitting }) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    };
    const body = {
      tour_id: tourDetail && tourDetail.tour_id ? tourDetail.tour_id : 0,

      location_name: !values.Location_name
        ? tourDetail.location.location_name
        : values.Location_name,

      tour_name: !values.Tour_name ? tourDetail.tour_name : values.Tour_name,

      price: !values.Price ? tourDetail.price : values.Price,

      number_days: !values.Days ? tourDetail.number_days : values.Days,

      number_nights: !values.Nights ? tourDetail.number_nights : values.Nights,

      introduce: !values.Introduce ? tourDetail.introduce : values.Introduce,

      description: !values.Description
        ? tourDetail.introduce
        : values.Description,
    };
    console.log(
      "🚀 ~ file: index.jsx ~ line 85 ~ handleCreateTour ~ body",
      body
    );

    try {
      const res = await axios.put(
        `http://localhost:5000/api/tours/update`,
        body,
        config
      );
      console.log(
        "🚀 ~ file: API.js ~ line 254 ~ handleCreateTour ~ res",
        res.data
      );
      setTimeout(() => {
        console.log("value : ", values);
        setSubmitting(false);
        // alert(res.status);
      }, 300);
      if (res.status === 201) setTourInfo(res.data.message);
    } catch (error) {
      if (error.response.status === 400)
        // alert(error.response.data.message);
        setNotify({
          message: error.response.data.message,
          type: "error",
        });

      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleUploadImg = async () => {
    const formData = await new FormData();

    // Update the formData object
    await formData.append("files", selectedFile);
    await formData.append(
      "name",
      `tour-${tourId}-org-${userAccount.account_id}`
    );

    try {
      const res = await axios.post(
        `http://localhost:5000/api/tours/images`,
        formData
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 38 ~ handleUploadImg ~ res",
        res.data
      );
      setTimeout(() => {
        setNotify({
          message: "Edit successfully",
          type: "success",
        });
        window.location.href = "/orgViewTour";
      }, 1000);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  const handleSubmitEdit = async (values, { setSubmitting }) => {
    await handleCreateTour(values, { setSubmitting });
    await handleUploadImg();
  };

  const onImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      setImage(URL.createObjectURL(e.target.files[0]));
      setSelectedFile(e.target.files[0]);
    }
  };

  return (
    <div className="postpage_container">
      <Formik
        // validationSchema={createTourValidationSchema}
        initialValues={initialTourValues}
        onSubmit={(values, { setSubmitting }) => {
          handleSubmitEdit(values, { setSubmitting });
        }}
      >
        {(formikProps) => {
          const { isSubmitting, handleChange } = formikProps;
          return (
            <Form className="post_side">
              <div className="post_title">
                <div className="user_avatar">
                  <div className="avatar">
                    {image ? (
                      <img
                        className="preview_img"
                        src={image}
                        alt="preview_image"
                      />
                    ) : (
                      <img
                        src={
                          `http://localhost:5000/api/tours/images?tour=${tourId}&org=${userAccount.account_id}`
                            ? `http://localhost:5000/api/tours/images?tour=${tourId}&org=${userAccount.account_id}`
                            : Images.EMPTY
                        }
                        alt="user_avt"
                      />
                    )}
                  </div>
                  <label className="avatar_change_icon">
                    <FontAwesomeIcon
                      className="icon"
                      icon={faCamera}
                      size="1x"
                      style={{ color: "#464646" }}
                    />
                    <input
                      onChange={onImageChange}
                      name="file_avt"
                      type="file"
                      required
                    />
                  </label>
                </div>

                <div className="post_name_price">
                  <div className="post_name">
                    <Field
                      name="Tour_name"
                      className="input"
                      type="text"
                      placeholder={
                        tourDetail && tourDetail.tour_name
                          ? tourDetail.tour_name
                          : "Please type your tour's name"
                      }
                      spellCheck="false"
                    />
                    <ErrorMessage name="Tour_name" />
                  </div>
                  <div className="post_price">
                    <Field
                      name="Price"
                      className="input"
                      type="text"
                      placeholder={
                        tourDetail && tourDetail.price
                          ? tourDetail.price
                          : "Please type your tour's price"
                      }
                      spellCheck="false"
                    />
                    <ErrorMessage name="Price" />
                  </div>
                  <div className="post_time">
                    <div className="post_time_day">
                      <Field
                        name="Days"
                        className="input"
                        type="text"
                        placeholder={
                          tourDetail && tourDetail.number_days
                            ? tourDetail.number_days
                            : " Days "
                        }
                        spellCheck="false"
                      />
                      <ErrorMessage name="Days" />
                    </div>

                    <div className="post_time_night">
                      <Field
                        name="Nights"
                        className="input"
                        type="text"
                        placeholder={
                          tourDetail && tourDetail.number_nights
                            ? tourDetail.number_nights
                            : " Nights "
                        }
                        spellCheck="false"
                      />
                      <ErrorMessage name="Nights" />
                    </div>
                  </div>

                  <div className="post_location">
                    <LocationOptions
                      tourDetail={tourDetail}
                      handleChange={handleChange}
                    />
                    <LocationCreated />
                  </div>

                  <div className="post_status">
                    <Button variant="outlined">Available</Button>
                    <Button variant="outlined">Temp</Button>
                    <Button variant="outlined">Disable</Button>
                  </div>
                </div>
              </div>

              <div className="post_body">
                <div className="post_introduce describe">
                  <h2>Please write an introduce for this trip</h2>
                  <Field
                    placeholder={
                      tourDetail && tourDetail.introduce
                        ? tourDetail.introduce
                        : ""
                    }
                    className="textarea"
                    name="Introduce"
                  />
                  <ErrorMessage name="Introduce" />
                </div>
                <div className="post_description describe">
                  <h2>Please write a description of this trip</h2>
                  <Field
                    placeholder={
                      tourDetail && tourDetail.description
                        ? tourDetail.description
                        : ""
                    }
                    className="textarea"
                    name="Description"
                  />
                  <ErrorMessage name="Description" />
                </div>
                <Button type="submit" variant="outlined">
                  {isSubmitting ? (
                    <CircularProgress size={21} color="success" />
                  ) : (
                    "Save"
                  )}
                </Button>
                <Notification notify={notify} />
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default EditPostPage;
