import { faCircleArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./OrgViewDetailTour.css";
import Cookies from "universal-cookie";
import { Images } from "../../constants/images";

OrgViewDetailTour.propTypes = {};

function OrgViewDetailTour(props) {
  const cookies = new Cookies();
  const userToken = cookies.get("token");
  const { tourId } = useParams();
  console.log(
    "🚀 ~ file: index.jsx ~ line 11 ~ OrgViewDetailTour ~ tourId",
    tourId
  );
  const navigate = useNavigate();

  var [orgViewList, setOrgViewList] = useState([]);
  var [orgViewTourDetail, setOrgViewTourDetail] = useState([]);

  useEffect(() => {
    getOrgViewDetailTour();
  }, []);

  const getOrgViewDetailTour = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    };
    try {
      const res = await axios.get(
        `http://localhost:5000/api/tours/org/number-of-orderers?tour=${tourId}`,
        config
      );
      console.log(
        "🚀 ~ file: index.jsx ~ line 25 ~ getOrgViewDetailTour ~ res",
        res.data.message
      );
      orgViewList = res.data.message.list;
      orgViewTourDetail = res.data.message.tour;

      setOrgViewList(orgViewList);
      setOrgViewTourDetail(orgViewTourDetail);
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  };

  return (
    <div className="org_view_detail_tour">
      <button className="ticket_back_btn ">
        <FontAwesomeIcon icon={faCircleArrowLeft} />
      </button>
      <div className="detail_tour_header">
        <span className="detail_tour_header_name">
          {orgViewTourDetail && orgViewTourDetail.tour_name
            ? orgViewTourDetail.tour_name
            : ""}
        </span>
        <span className="detail_tour_header_price">
          ${orgViewTourDetail.price}
        </span>
        <span className="detail_tour_header_loca">
          {`${
            orgViewTourDetail && orgViewTourDetail.location
              ? orgViewTourDetail.location.country.country_name
              : ""
          } - ${
            orgViewTourDetail && orgViewTourDetail.location
              ? orgViewTourDetail.location.location_name
              : ""
          }`}{" "}
        </span>
        <button
          onClick={() =>
            navigate(
              `/detail/${orgViewTourDetail.tour_id}/${orgViewTourDetail.org_id}`
            )
          }
        >
          Detail
        </button>
      </div>
      <div className="detail_tour_body">
        <div className="detail_tour_body_container">
          {orgViewList && orgViewList.length === 0 && (
            <div className="detail_tour_null">
              <img src={Images.EMPTY} alt="" />
              No one have booked this tour yet!!🤔
            </div>
          )}

          {orgViewList && orgViewList.length !== 0 && (
            <table>
              <tr>
                <th>Date</th>
                <th>Amount of orders</th>
              </tr>
              {orgViewList.map((item, key) => {
                return (
                  <tr key={key}>
                    <td>{item.booked_time.substring(0, 10)} </td>
                    <td>{item.orderers} </td>
                  </tr>
                );
              })}
            </table>
          )}

          {/* <tr>
              <td>2022-03-23</td>
              <td>100</td>
            </tr>
            <tr>
              <td>2022-04-23</td>
              <td>200</td>
            </tr> */}
        </div>
      </div>
    </div>
  );
}

export default OrgViewDetailTour;
