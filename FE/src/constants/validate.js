import * as Yup from "yup";

// logic
export const initialTourValues = {
  Tour_name: "",
  Location_name: "",
  Price: "",
  Days: "",
  Nights: "",
  Introduce: "",
  Description: "",
};

export const initialLoginValues = {
  Email: "",
  Password: "",
};

export const initialRegistUserValues = {
  Username: "",
  Email: "",
  Password: "",
  ConfirmPassword: "",
  IsAgree: false,
  Mobile: "",
  Country: "",
};

export const initialRegistOrgValues = {
  Username: "",
  Email: "",
  Password: "",
  ConfirmPassword: "",
  IsAgree: false,
  Mobile: "",
  Address: "",
  Country: "",
  ConfirmCode: "",
  BusinessLicense: null,
};

export const initialSearchValues = {
  keyword: "",
};

export const initialCountryOptionValues = {
  Location_name: "",
  Country: "",
};

export const initialBookTourValues = {
  Number_tourists: 1,
  Booked_time: "",
};

// login validate
export const loginValidationSchema = Yup.object().shape({
  Email: Yup.string()
    .email("Invalid email address")
    .required("This field is required"),

  Password: Yup.string()
    .min(6, "Must be at least 6 character or equal")
    .max(30, "Must be 30 character or less")
    .required("This field is required"),
});

// user register validate
export const registUserValidationSchema = Yup.object().shape({
  Username: Yup.string()
    .min(2, "Invalid")
    .max(30, "Invalid")
    .required("This field is required"),
  // .matches(/^[a-zA-Z0-9]+$/, "Cannot contain special characters or spaces"),

  Password: Yup.string()
    .min(6, "Must be at least 6 character or equal")
    .max(30, "Must be 30 character or less")
    .required("This field is required"),

  ConfirmPassword: Yup.string()
    .min(6, "Must be at least 6 character or equal")
    .max(30, "Must be 30 character or less")
    .required("This field is required"),

  Email: Yup.string()
    .email("Invalid email address")
    .required("This field is required"),

  IsAgree: Yup.boolean()
    .required("This field is required")
    .oneOf([true], "You must accept the license."),

  Mobile: Yup.string()
    .required("This field is required")
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Your phone number is not available"
    ),
  Country: Yup.string().required("Select Country"),
});

// organization register validate
export const registOrgValidationSchema = Yup.object().shape({
  Username: Yup.string()
    .min(2, "Invalid")
    .max(30, "Invalid")
    .required("This field is required"),
  // .matches(/^[a-zA-Z0-9]+$/, "Cannot contain special characters or spaces"),

  Password: Yup.string()
    .min(6, "Must be at least 6 character or equal")
    .max(30, "Must be 30 character or less")
    .required("This field is required"),

  Email: Yup.string()
    .email("Invalid email address")
    .required("This field is required"),

  IsAgree: Yup.boolean()
    .required("This field is required")
    .oneOf([true], "You must accept the terms and conditions."),

  Mobile: Yup.string()
    .required("This field is required")
    .matches(
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
      "Your phone number is not available"
    ),

  Address: Yup.string()
    .min(2, "Invalid")
    .max(30, "Invalid")
    .required("This field is required"),

  // Country: Yup.number().required("This field is required!!").nullable(),

  BusinessLicense: Yup.number().required("This field is required!!").positive(),

  ConfirmCode: Yup.string().required("This field is required"),

  Country: Yup.string().required("Select Country"),
});

export const createTourValidationSchema = Yup.object().shape({
  Tour_name: Yup.string()
    .min(6, "Must be at least 6 character or equal")
    .max(50, "Must be 30 character or less")
    .required("This field is required"),

  Price: Yup.number().positive().required("This field is required"),

  Days: Yup.string()
    .min(1, "Must be at least 6 character or equal")
    .max(10, "Must be 10 character or less")
    .required("This field is required"),

  Nights: Yup.string()
    .min(1, "Must be at least 6 character or equal")
    .max(10, "Must be 10 character or less")
    .required("This field is required"),

  Introduce: Yup.string()
    .min(10, "Must be at least 10 character or equal")
    .max(100, "Must be 100 character or less")
    .required("This field is required"),

  Description: Yup.string()
    .min(10, "Must be at least 10 character or equal")
    .max(100, "Must be 100 character or less")
    .required("This field is required"),

  Location_name: Yup.string().required("Please select location"),
});

export const createLocationSchema = Yup.object().shape({
  Location_name: Yup.string()
    .min(2, "Invalid")
    .max(30, "Invalid")
    .required("This field is required"),
  Country: Yup.string().required("Select Country"),
});

export const bookTourSchema = Yup.object().shape({
  Number_tourists: Yup.number().positive().required("This field is required!!"),
  Booked_time: Yup.string()
    .min(8, "Invalid")
    .required("This field is required"),
});
