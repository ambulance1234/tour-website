import { useState, useEffect } from "react";
import axios from "axios";

axios.defaults.baseURL = "http://localhost:5000/api";

export const useAxios = ({ url, method, body = null, headers = null }) => {
  // console.log(
  //   "🚀 ~ file: fetchAPI.js ~ line 7 ~ useAxios ~ url, method",
  //   url,
  //   method
  // );
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getData();
  }, [method, url]);

  // const getData = async () => {
  //   try {
  //     const res = await axios[method](url);
  //     console.log("🚀 ~ file: fetchAPI.js ~ line 23 ~ getData ~ res", res);
  //     setResponse(res.data);
  //   } catch (error) {
  //     console.log(error.response.data);
  //     console.log(error.response.status);
  //     console.log(error.response.headers);
  //   }
  // };
  const getData = () => {
    axios[method](url)
      .then((res) => {
        setResponse(res.data);
      })
      .catch((err) => {
        setError(err);
      });
    // .finally(() => {
    //     setloading(false);
    // });
  };
  return { response, error, loading };
};
