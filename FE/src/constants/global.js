export const scrollTop = () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
};

export const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

export var settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 3,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

export const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export const calculateTime = (hour) => {
  switch (hour) {
    case "12":
      return "1 days";

    case "36":
      return "2 days 1 nights";

    case "60":
      return "3 days 2 nights";

    case "132":
      return "5 days 4 nights";

    default:
      return "";
  }
};

export const calculateBookMonth = (month) => {
  switch (month) {
    case "Jan":
      return 1;

    case "Feb":
      return 2;

    case "Mar":
      return 3;

    case "Apr":
      return 4;

    case "May":
      return 5;

    case "Jun":
      return 6;

    case "Jul":
      return 7;

    case "Aug":
      return 8;

    case "Sep":
      return 9;

    case "Oct":
      return 10;

    case "Nov":
      return 11;

    default:
      return 12;
  }
};
