export const country = [
  { value: 1, label: "VN" },
  { value: 2, label: "BR" },
  { value: 3, label: "CA" },
  { value: 4, label: "CL" },
  { value: 5, label: "CN" },
  { value: 6, label: "CO" },
  { value: 7, label: "CG" },
  { value: 8, label: "CU" },
  { value: 9, label: "ID" },
  { value: 10, label: "IT" },
  { value: 11, label: "JP" },
  { value: 12, label: "KR" },
];
