import jokerKATM from "../assets/images/thailan.jpg";
import handwash from "../assets/images/illustration-handwash_2x.png";
import logo from "../assets/images/logo.jpg";
import london from "../assets/images/london.jpg";
import lourve from "../assets/images/lourve.jpg";
import lourve2 from "../assets/images/louvre2.jpg";
import muine from "../assets/images/muine.jpg";
import palais from "../assets/images/Palais.jpg";
import thailan from "../assets/images/thailan.jpg";
import vinhhalong from "../assets/images/vinhhalong.jpg";
import vungtau from "../assets/images/vungtau.jpg";
import beach from "../assets/images/beach_banner.jpg";
import eiffel from "../assets/images/eiffel.jpg";
import ticket from "../assets/images/Ticket.png";
import empty from "../assets/images/empty.png";

export const Images = {
  EMPTY: empty,
  JOKER: jokerKATM,
  HANDWASH: handwash,
  LOGO: logo,
  LONDON: london,
  LOURVE: lourve,
  LOURVE2: lourve2,
  MUINE: muine,
  PALAIS: palais,
  THAILAN: thailan,
  VINHHALONG: vinhhalong,
  VUNGTAU: vungtau,
  BEACH: beach,
  EIFFEL: eiffel,
  TICKET: ticket,
};
