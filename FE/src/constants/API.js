import axios from "axios";
import Cookies from "universal-cookie";
import { calculateBookMonth } from "./global";

const cookies = new Cookies();
const userToken = cookies.get("token");

export const handleLogout = async () => {
  await cookies.remove("account");
  await cookies.remove("token");

  // if()
  window.location.href = await "/home";
};

export const handleLoginSubmit = async (
  values,
  { setSubmitting },
  { setNotify }
) => {
  const body = {
    email: values.Email,
    password: values.Password,
  };

  try {
    const res = await axios.post(
      "http://localhost:5000/api/gateway/login",
      body
    );

    const role = res.data.message.information.role_id;

    if (res.data.message) {
      const data = res.data.message;
      await saveCookie(data);
    }
    setTimeout(() => {
      setSubmitting(false);
      setNotify({
        message: "Login successfully!!",
        type: "success",
      });
      if (role === 1) window.location.href = "/adminPage";
      if (role === 2) window.location.href = "/orgViewTour";
      if (role === 3) window.location.href = "/home";
    }, 1000);
  } catch (error) {
    setNotify({
      message: error.response.data.message,
      type: "error",
    });
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleRegisterUserSubmit = async (
  values,
  { setSubmitting },
  { setNotify }
) => {
  const body = {
    username: values.Username,
    email: values.Email,
    password: values.Password,
    phone: values.Mobile,
    role: "USER",
    country: values.Country,
  };

  try {
    const res = await axios.post(
      "http://localhost:5000/api/accounts/users/register",
      body
    );
    console.log(
      "🚀 ~ file: api.js ~ line 75 ~ handleRegisterUserSubmit ~ res",
      res.data
    );
    setTimeout(() => {
      setSubmitting(false);
      setNotify({
        message: "Register for user successfully!!",
        type: "success",
      });
      window.location.href = "/home";
    }, 1000);
  } catch (error) {
    setNotify({
      message: error.response.data.message,
      type: "error",
    });
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleRegisterOrgSubmit = async (
  values,
  { setSubmitting },
  { setNotify }
) => {
  const body = {
    org_name: values.Username,
    address: values.Address,
    business_license: values.BusinessLicense,
    confirm_code: values.ConfirmCode,
    email: values.Email,
    password: values.Password,
    phone: values.Mobile,
    role: "ORGANIZATION",
    country: values.Country,
  };
  console.log(
    "🚀 ~ file: validate.js ~ line 93 ~ handleRegisterOrgSubmit ~ body",
    body
  );

  try {
    const res = await axios.post(
      "http://localhost:5000/api/accounts/orgs/register",
      body
    );
    console.log(
      "🚀 ~ file: validate.js ~ line 47 ~ handleLoginSubmit ~ res",
      res.data
    );
    setTimeout(() => {
      setSubmitting(false);
      setNotify({
        message: "Regist for Organize successfully",
        type: "success",
      });
      window.location.href = "/home";
    }, 1000);
  } catch (error) {
    setNotify({
      message: error.response.data.message,
      type: "error",
    });
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleSearchOrganize = async (values, { setSubmitting }) => {
  const body = {
    org_name: values.keyword,
  };
  console.log("🚀 ~ file: API.js ~ line 7 ~ handleSearch ~ body", body);
  try {
    const res = await axios.post(
      "http://localhost:5000/api/accounts/orgs/search",
      body
    );
    console.log(
      "🚀 ~ file: validate.js ~ line 47 ~ handleLoginSubmit ~ res",
      res.data
    );
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleCreateLocation = async (
  values,
  { setSubmitting },
  { setNotify }
) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${userToken}`,
    },
  };
  const body = {
    location_name: values.Location_name,
    country_code: values.Country,
  };

  try {
    const res = await axios.post(
      "http://localhost:5000/api/countries/locations/create",
      body,
      config
    );
    console.log(
      "🚀 ~ file: validate.js ~ line 47 ~ createCountry ~ res",
      res.data
    );
    setTimeout(() => {
      console.log("value : ", values);
      setSubmitting(false);
      setNotify({
        message: "Create location successfully!!",
        type: "success",
      });

      window.location.reload();
    }, 1000);
  } catch (error) {
    setNotify({
      message: error.response.data.message,
      type: "error",
    });
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleBookTour = async (
  values,
  { setSubmitting },
  numberOfPeople,
  { setNotify }
) => {
  const bookDate = new Date(values.Booked_time);

  const dateString = String(bookDate);

  const bookMonth = calculateBookMonth(dateString.substring(4, 7));
  const bookYear = dateString.substring(11, 15);

  const updatedBookDate = `${bookYear}-${bookMonth}-${bookDate.getDate()}`;

  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
    },
  };
  const tourId = Number(localStorage.getItem("tourId"));
  const orgId = Number(localStorage.getItem("orgId"));

  const body = {
    tour_id: tourId,
    org_id: orgId,
    number_tourists: numberOfPeople,
    booked_time: updatedBookDate,
  };

  try {
    const res = await axios.post(
      "http://localhost:5000/api/accounts/users/book-tour",
      body,
      config
    );
    console.log(
      "🚀 ~ file: api.js ~ line 254 ~ handleBookTour ~ res",
      res.data
    );
    setTimeout(() => {
      setSubmitting(false);
      setNotify({
        message: "Book tour successfully",
        type: "success",
      });
      // alert(res.statusText);
      window.location.href = `/ticket/${res.data.message.bookedTour.cart_id}`;
    }, 1000);
  } catch (error) {
    if (error.response.status === 400)
      setNotify({
        message: error.response.data.message,
        type: "error",
      });
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const handleCancelTour = async (cartId, { setNotify }) => {
  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
    },
  };
  try {
    const res = await axios.delete(
      `http://localhost:5000/api/accounts/users/cancel-tour?cart=${cartId}`,
      config
    );
    console.log(
      "🚀 ~ file: index.jsx ~ line 55 ~ handleCancelTour ~ res",
      res.data
    );
    setTimeout(() => {
      setNotify({
        message: res.statusText,
        type: "success",
      });
      window.location.href = `/`;
    }, 1000);
  } catch (error) {
    if (error.response.status === 400)
      setNotify({
        message: error.response.data.message.note,
        type: "error",
      });

    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getUserAccount = async (
  userAccountList,
  { setUserAccountList }
) => {
  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
    },
  };
  try {
    const res = await axios.get(
      `http://localhost:5000/api/accounts/users`,
      config
    );
    console.log(
      "🚀 ~ file: index.jsx ~ line 26 ~ getUser ~ res",
      res.data.message
    );

    userAccountList = res.data.message;
    setUserAccountList(userAccountList);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getOrgAccount = async (orgAccountList, { setOrgAccountList }) => {
  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
    },
  };
  try {
    const res = await axios.get(
      `http://localhost:5000/api/accounts/orgs`,
      config
    );
    console.log(
      "🚀 ~ file: index.jsx ~ line 52 ~ getOrgAccount ~ res",
      res.data.message
    );

    orgAccountList = res.data.message;
    setOrgAccountList(orgAccountList);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getCountries = async (countriesList, { setCountriesList }) => {
  try {
    const res = await axios.get("http://localhost:5000/api/countries");
    console.log(
      "🚀 ~ file: index.jsx ~ line 16 ~ getCountries ~ res",
      res.data
    );

    countriesList = res.data.message;
    setCountriesList(countriesList);
  } catch (error) {
    if (error && error.response) console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getLocation = async (locationList, { setLocationList }) => {
  try {
    const res = await axios.get(
      "http://localhost:5000/api/countries/locations"
    );
    console.log("🚀 ~ file: API.js ~ line 186 ~ getLocation ~ res", res.data);
    setLocationList(res.data.message);
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getTopData = async (suggestionList, { setSuggestionList }) => {
  try {
    const res = await axios.get(`http://localhost:5000/api/tours/top`);
    suggestionList = await res.data.message;
    setSuggestionList(suggestionList);
  } catch (error) {
    if (error && error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

export const getLoveData = async (loveList, { setLoveList }) => {
  try {
    const res = await axios.get(
      `http://localhost:5000/api/tours/search/name?keyword=Beach`
    );

    loveList = res.data.message.rows;
    setLoveList(loveList);
    console.log(
      "🚀 ~ file: index.jsx ~ line 30 ~ getLoveData ~ loveList",
      loveList
    );
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getUserCart = async (userCartList, { setUserCartList }) => {
  const config = {
    headers: {
      Authorization: `Bearer ${userToken}`,
    },
  };
  try {
    const res = await axios.get(
      `http://localhost:5000/api/accounts/users/cart`,
      config
    );
    userCartList = res.data.message.rows;
    setUserCartList(userCartList);
    console.log(
      "🚀 ~ file: index.jsx ~ line 61 ~ getUserCart ~ userCartList",
      userCartList
    );
  } catch (error) {
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  }
};

export const getTourDetail = async (tourDetail, { setTourDetail }) => {
  const tourId = Number(localStorage.getItem("tourId"));
  const orgId = Number(localStorage.getItem("orgId"));
  try {
    const res = await axios.get(
      `http://localhost:5000/api/tours/detail?tour=${tourId}&org=${orgId}`
    );
    console.log(
      "🚀 ~ file: index.jsx ~ line 30 ~ getTourDetail ~ res",
      res.data.message
    );
    tourDetail = res.data.message;
    setTourDetail(tourDetail);
  } catch (error) {
    if (error && error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }
  }
};

const saveCookie = (data) => {
  const { information, accessToken } = data;
  console.log("🚀 ~ file: API.js ~ line 257 ~ saveCookie ~ data", data);

  cookies.set("token", accessToken, {
    path: "/",
    maxAge: 7 * 24 * 60 * 60 * 1000,
  });

  cookies.set("account", information, {
    path: "/",
    maxAge: 7 * 24 * 60 * 60 * 1000,
  });
};
