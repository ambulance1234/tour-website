const initialState = {
  searchData: [],
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SEARCH_LIST": {
      return {
        ...state,
        searchData: action.payload,
      };
    }

    default:
      return state;
  }
};

export default searchReducer;
