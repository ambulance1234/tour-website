const initialState = {
  country: null,
};

export const registerOrganize = (state = initialState, action) => {
  switch (action.type) {
    case "COUNTRY":
      return {
        ...state,
        country: action.payload,
      };

    default:
      return state;
  }
};
