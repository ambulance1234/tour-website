const initialValues = {
  reviewsData: null,
};

export const detailReducer = (state = initialValues, action) => {
  switch (action.type) {
    case "REVIEW_DATA": {
      console.log(
        "🚀 ~ file: Detail.js ~ line 11 ~ detailReducer ~ action.payload",
        action.payload
      );
      return {
        ...state,
        reviewsData: action.payload,
      };
    }

    default:
      return state;
  }
};
