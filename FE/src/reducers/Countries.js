const initialState = {
  countriesList: [],
};

export const countriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "COUNTRY_LIST": {
      // console.log(
      //   "🚀 ~ file: Countries.js ~ line 11 ~ countriesReducer ~ action.payload",
      //   action.payload
      // );
      return {
        ...state,
        countriesList: action.payload,
      };
    }

    default:
      return state;
  }
};
