const initialState = {
  value: null,
  ticketId: null,
};

const countReducer = (state = initialState, action) => {
  switch (action.type) {
    case "INCREASE": {
      return {
        ...state,
        value: action.payload + 1,
      };
    }

    case "DECREASE": {
      return {
        ...state,
        value: action.payload - 1,
      };
    }

    case "TICKET": {
      return {
        ...state,
        ticketId: action.payload,
      };
    }

    default:
      return state;
  }
};

export default countReducer;
