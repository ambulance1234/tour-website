const initialValues = {
  token: localStorage.getItem("token"),
};

const loginReducer = (state = initialValues, action) => {
  switch (action.type) {
    case "SAVE_TOKEN": {
      console.log(
        "🚀 ~ file: Login.js ~ line 11 ~ loginReducer ~ action.payload",
        action.payload
      );
      return {
        ...state,
        token: localStorage.setItem("token", action.payload),
      };
    }

    default:
      return state;
  }
};

export default loginReducer;
