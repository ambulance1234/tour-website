import { combineReducers } from "redux";
import countReducer from "./Count";
import { countriesReducer } from "./Countries";
import { detailReducer } from "./Detail";
import loginReducer from "./Login";
import { registerOrganize } from "./RegisterOrg";
import searchReducer from "./Search";

const rootReducer = combineReducers({
  registerOrg: registerOrganize,
  login: loginReducer,
  detail: detailReducer,
  countriesList: countriesReducer,
  search: searchReducer,
  count: countReducer,
});

export default rootReducer;
