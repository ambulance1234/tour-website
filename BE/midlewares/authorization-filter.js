require("dotenv").config();
const jwt = require("jsonwebtoken");
const StatusCode = require("../application/status-code");
const roleServices = require("../services/account_mgm/role-services");

const roleAble = {
  ADMIN: "ADMIN",
  ORGANIZATION: "ORGANIZATION",
  USER: "USER",
};

const claimTokenFromHeader = (req) => {
  var authenHeader = req.header("Authorization");
  var token = authenHeader && String(authenHeader).split(" ")[1];
  if (!token) return null;
  else return token;
};

const verifyAccess = (req) => {
  var token = claimTokenFromHeader(req);
  if (!token) {
    return token;
  } else {
    try {
      var tokenDecoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
      console.log(tokenDecoded);

      return tokenDecoded;
    } catch (err) {
      console.log(err);
      console.log(err.name);
      // res.status(403).json("Forbidden");
      if (err.name === "TokenExpiredError") {
        return "Expired";
      }
      return false;
    }
  }
};

const acceptRoles = async (req, res, acceptedRoles, next) => {
  const payload = verifyAccess(req);
  if (!payload)
    return res
      .status(401)
      .json({ message: "Missing authorize information. Access denied" });
  if (String(payload) === "Expired")
    return res.status(403).json({ message: payload });

  let roleReadFromDB = await roleServices.getById(payload.role_id);
  console.log("role is " + roleReadFromDB.role_name);
  const isGivenRoleAllowed = acceptedRoles.find((ele) => {
    return String(roleReadFromDB.role_name).toUpperCase() === ele;
  });
  if (isGivenRoleAllowed) {
    req.user = payload;
    next();
  } else {
    return res
      .status(403)
      .json({ message: "Out of Permission range. Access denied" });
  }
};

const acceptUserOnly = async (req, res, next) => {
  await acceptRoles(req, res, [roleAble.USER], next);
};
const acceptOrgOnly = async (req, res, next) => {
  await acceptRoles(req, res, [roleAble.ORGANIZATION], next);
};
const acceptAdminOnly = async (req, res, next) => {
  await acceptRoles(req, res, [roleAble.ADMIN], next);
};
const acceptAdminOrg = async (req, res, next) => {
  await acceptRoles(req, res, [roleAble.ADMIN, roleAble.ORGANIZATION], next);
};
const acceptAllRoles = async (req, res, next) => {
  await acceptRoles(req, res, [roleAble.USER, roleAble.ORGANIZATION, roleAble.ADMIN], next);
}
module.exports = {
  acceptAdminOnly,
  acceptOrgOnly,
  acceptUserOnly,
  acceptAdminOrg,
  acceptAllRoles
};
