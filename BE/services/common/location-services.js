const locations = require("../../config/model-instance").locations;
const countries = require("../../config/model-instance").countries;
const commonErrors = require("../../application/errors/common-errors");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

//getAll
const getAll = async () => {
  return await locations.findAll();
};
//getByName: 1
const getLocationByName = async (name) => {
  return await locations.findOne({
    where: {
      location_name: name,
    },
    include: [
      {
        model: countries,
        as: "country",
        attributes: ["country_code", "country_name"],
        require: true,
      },
    ],
  });
};
//search locations
const searchLocations = async (keyword) => {
  return await locations.findAll({
    where: {
      location_name: {
        [Op.like]: `%${keyword}%`,
      },
    },
    include: [
      {
        model: countries,
        as: "country",
        attributes: ["country_code", "country_name"],
        require: true,
      },
    ],
  });
};
//getByCountry
const getByCountry = async (country_code) => {
  const country = await countries.findOne({
    where: {
      country_code,
    },
  });
  return await locations.findAll({
    where: {
      country_id: country.country_id,
    },
  });
};
//create new location
// {
//     location_name,
//     country_code
// }
const createNewLocation = async (location) => {
  if (!location.location_name) throw commonErrors.InvalidInformationError;
  const country = await countries.findOne({
    where: { country_code: location.country_code },
  });

  //The given location name may be existed
  //Look up for the existed
  //If the existed has the same name but different country => accept
  //Else, deny
  const existedOne = await getLocationByName(location.location_name);
  if(existedOne) {
    if(existedOne.country_id === country.country_id) {
      throw commonErrors.DataExistedError;
    }
  }

  const created = await locations.create({
    location_name: location.location_name,
    country_id: country.country_id,
  });
  created.country_code = country.country_code;
  return created;
};

module.exports = {
  getAll,
  getByCountry,
  getLocationByName,
  createNewLocation,
  searchLocations
};
