const countries = require("../../config/model-instance").countries;
const { Op } = require("sequelize");

const columns = ["country_id", "country_code", "country_name"];

const getAll = async () => {
  let list = await countries.findAll({
    attributes: columns,
  });
  return list;
};

const verifyCtrCode = (code) => {
  //   console.log(code);
  if (typeof code !== "string") {
    return false;
  } else {
    let pattern = /[0-9]/g;
    if (code.match(pattern) === null) {
      return true;
    }
    return false;
  }
};

const getByIdOrCode = async (id, code) => {
  code = verifyCtrCode(code) === true ? code : "";
  let country = await countries.findOne({
    attributes: columns,
    where: {
      [Op.or]: [
        { country_id: id === undefined ? null : id },
        { country_code: code },
      ],
    },
  });
  // console.log(country);
  // console.log(country.country_id);
  return country;
};

module.exports = { getAll, getByIdOrCode };
