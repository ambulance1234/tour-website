const md5 = require("md5");
const {
  admins,
  accounts,
  countries,
  end_users,
  organizations,
} = require("../../config/model-instance");

const getAllAdmins = async (page = 1, limit = 10) => {
  const offset = (page - 1) * limit;
  const list = await admins.findAndCountAll({
    include: [
      {
        model: accounts,
        as: "account",
        require: true,
        attributes: ["email", "phone"],
      },
    ],
    offset,
    limit,
  });
  return list;
};

const getSpecifyAdmin = async (account_id) => {
  const detail = await admins.findOne({
    where: { account_id },
    include: [
      {
        model: accounts,
        as: "account",
        require: true,
        attributes: ["email", "phone"],
        include: [{ model: countries, as: "country", require: true }],
      },
    ],
  });
  return detail;
};

/*
{
    account_id
    username
    secret_key
}
 */
const register = async (account, adminInfo) => {
  let tempUser = {
    account_id: account.account_id,
    username: adminInfo.username,
    age: 17,
    address: "Unknown",
    fullname: "King of the Jungle",
    gender: "Unknown",
    secret_key: md5(adminInfo.secret_key),
  };

  let findExist = await getSpecifyAdmin(tempUser.account_id);
  if (findExist !== null) {
    throw commonErrors.DataExistedError;
  }
  let newUser = await admins.create(tempUser);
  let createdAdmin = {
    username: newUser.username,
    fullname: newUser.fullname,
    gender: newUser.gender,
    email: account.email,
    phone: account.phone,
    role_id: account.role_id,
    country_id: account.country_id,
  };
  return createdAdmin;
};

const checkSecretKey = async (account_id, secret_key) => {
  const admin = await getSpecifyAdmin(account_id);
  if (admin.secret_key === md5(secret_key)) return true;
  else return false;
};

const deleteUser = async (account_id) => {
  const findUser = await end_users.findOne({ where: { account_id } });
  const delUser = await findUser.destroy();
  return delUser;
};

const deleteOrg = async (account_id) => {
  const findOrg = await organizations.findOne({ where: { account_id } });
  const delOrg = await findOrg.destroy();
  return delOrg;
};

const deleteAccount = async (account_id) => {
  const findAccount = await accounts.findOne({ where: { account_id } });
  const delAcc = await findAccount.destroy();
  return delAcc;
};

const deleteUserAction = async (admin, user, target) => {
  if ((await checkSecretKey(admin.account_id, admin.secret_key)) === false) {
    throw "KeyNotCorrectError";
  }
  let isDelTarget = 0;
  switch (target) {
    case "USER":
      isDelTarget = await deleteUser(user.account_id);
      break;
    case "ORG":
      isDelTarget = await deleteOrg(user.account_id);
      break;
    default:
        throw "InvalidTargetError";
  }
  console.log("=====================DELETED USER=================");
  console.log(isDelTarget);
  const isDelAcc = await deleteAccount(user.account_id);
  return isDelAcc;
};

module.exports = {
  getAllAdmins,
  getSpecifyAdmin,
  register,
  checkSecretKey,
  deleteUserAction,
};
