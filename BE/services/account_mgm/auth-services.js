require("dotenv").config();
const jwt = require("jsonwebtoken");
const accountService = require("./account-services");
const md5 = require("md5");

const modifyPayload = (account) => {
  return {
    account_id: account.account_id,
    email: account.email,
    phone: account.phone,
    role_id: account.role_id,
  };
};

const accessExpireIn = "23h";
const resfreshExprireIn = "1h";

//This function will generate the token (both for access and resfresh tokens)
const generateToken = (payload, timeExpired) => {
  let accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: timeExpired,
  });
  return accessToken;
};

const login = async (information) => {
  if (typeof information !== "object") {
    return false;
  }
  console.log(information);
  let existedUser = await accountService.getByEmail(information.email);
  console.log(existedUser);
  if (!existedUser) {
    return "Information has not existed";
  }
  information.password = md5(information.password);
  if (existedUser.password !== information.password) {
    return "Password is not correct";
  }
  let modifiedUser = modifyPayload(existedUser);
  let userToken = {
    information: modifiedUser,
    accessToken: generateToken(modifiedUser, accessExpireIn),
  };
  return userToken;
};

const logout = (info) => {
  let userToken = {
    information: {
      account_id: 0,
      email: "guest@mail.com",
      phone: "0",
      role_id: 0,
    },
    accessToken: generateToken(info, "0s"),
  };
  return userToken;
};

module.exports = {
  generateToken,
  login,
  logout,
};
