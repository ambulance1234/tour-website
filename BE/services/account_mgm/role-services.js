const roles = require('../../config/model-instance').roles;

const getAll = async () => {
    let list = await roles.findAll();
    return list;
}

const getById = async (roleId) => {
    let roleRecord = await roles.findOne({
        attributes: ["role_name"],
        where: {
            role_id: Number(roleId)
        }
    });
    return roleRecord;
}

module.exports = {
    getAll,
    getById
};
