const sequelize = require("sequelize");
const Op = sequelize.Op;
const commonErrors = require("../../application/errors/common-errors");

const organizations = require("../../config/model-instance").organizations;
const accounts = require("../../config/model-instance").accounts;
const countries = require("../../config/model-instance").countries;

const md5 = require('md5');

const getAllOrgs = async () => {
  let orgs = await organizations.findAll();
  return orgs;
};

//get exactly 1 organization with name
const getOrgByName = async (orgName) => {
  let org = await organizations.findOne({ where: { org_name: orgName } });
  return org;
};

const getOrgByIdOrBLicense = async (condition) => {
  let org = await organizations.findOne({
    where: condition,
    include: [
      {
        model: accounts,
        as: "account",
        attributes: ["email", "phone", "role_id", "country_id"],
        require: true,
      },
    ],
  });
  return org;
};

const orgRegister = async (account, orgInfo) => {
  let tempOrg = {
    account_id: account.account_id,
    org_name: orgInfo.org_name,
    confirm_code: md5(orgInfo.confirm_code),
    address: orgInfo.address,
    postal_code: "Unknown",
    business_license: orgInfo.business_license,
    warning: 0,
  };

  let findExist = await getOrgByName(tempOrg.org_name);
  if (findExist !== null) {
    throw "OrgExistedError";
  }

  let newOrg = await organizations.create(tempOrg);
  let createdOrg = {
    org_name: newOrg.org_name,
    confirm_code: orgInfo.confirm_code,
    address: newOrg.address,
    postal_code: newOrg.postal_code,
    business_license: newOrg.business_license,
    email: account.email,
    phone: account.phone,
    role_id: account.role_id,
    country_id: account.country_id,
  };
  return createdOrg;
};

const updateOrgInfo = async (account_id, newInfo) => {
  let findExist = await getOrgByName(newInfo.org_name);

  if (findExist === null) {
    throw commonErrors.DataNotExistedError;
  }

  console.log(`account_id: ${account_id}`);
  console.log(newInfo);
  console.log(findExist);

  if (findExist.account_id !== account_id) {
    console.log('error in comapare account');
    throw commonErrors.InformationNotMatchedError;
  }

  if (md5(newInfo.confirm_code) !== findExist.confirm_code) {
    console.log('error in comapare code');
    throw commonErrors.InformationNotMatchedError;
  }

  findExist.set(newInfo);
  let isSave = await findExist.save();
  return isSave;
};

//updateCodeInfo = {account_id, oldCCode, newCCode, password}
const changeConfirmCode = async (updateCodeInfo) => {
  let existedOrg = await getOrgByIdOrBLicense({
    account_id: updateCodeInfo.account_id,
  });
  if (!existedOrg) {
    throw commonErrors.DataNotExistedError;
  }
  if (existedOrg.confirm_code !== md5(updateCodeInfo.oldCCode)) {
    throw commonErrors.InformationNotMatchedError;
  }
  let accountInfo = await accounts.findOne({
    where: {
      account_id: existedOrg.account_id,
    },
  });
  if (accountInfo.password !== md5(updateCodeInfo.password)) {
    throw commonErrors.InformationNotMatchedError;
  }
  existedOrg.set({
    confirm_code: updateCodeInfo.newCCode,
  });
  let isSave = await existedOrg.save();
  return isSave;
};

//search organizations which contain info which matched with query
const searchOrgs = async (searchQuery) => {
  let list = await organizations.findAll({
    attributes: [
      "org_name",
      "address",
      "postal_code",
      "business_license",
      "warning"
    ],
    where: {
      [Op.or]: [
        {
          org_name: {
            [Op.like]: `%${searchQuery.org_name}%`,
          },
        },
        {
          business_license: {
            [Op.like]: `%${searchQuery.business_license}%`,
          },
        },
      ],
    },
    include: [
      {
        model: accounts,
        as: "account",
        attributes: ["email", "phone", "country_id"],
        require: true,
        include: [
          {
            model: countries,
            as: "country",
            require: true,
          },
        ]
      },
    ],
  });
  return list;
};

module.exports = {
  getAllOrgs,
  getOrgByIdOrBLicense,
  orgRegister,
  updateOrgInfo,
  changeConfirmCode,
  searchOrgs
};
