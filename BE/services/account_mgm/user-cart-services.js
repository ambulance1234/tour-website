const commonErrors = require("../../application/errors/common-errors");
const {
  compareTime,
  totalDateFromTo,
  newDateFromHours,
  getNow,
} = require("../../application/time");
const {
  carts,
  tours,
  locations,
  countries,
  organizations,
  accounts,
} = require("../../config/model-instance");
const sequelize = require("sequelize");
const Op = sequelize.Op;

const getCartOfUser = async (user_id, page = 1, limit = 3) => {
  let offset = (page - 1) * limit;
  const cart = await carts.findAndCountAll({
    where: { user_id },
    offset,
    limit,
  });
  return cart;
};

/*
user: user_id [x]
tour: {
    tour_id,
    org_id,
    number_tourists,
    booked_time
}
Check booked time
*/

const bookTour = async (user_id, tour) => {
  const findTour = await tours.findOne({
    where: { tour_id: tour.tour_id, org_id: tour.org_id },
  });
  if (!findTour) {
    return { ok: false, err: 404, msg: "Tour not found" };
  }
  if (findTour.status_id !== 1) {
    return { ok: false, err: 404, msg: "Tour is not available" };
  }

  const cptime = compareTime(new Date(String(tour.booked_time)), new Date());
  console.log(cptime);
  if (cptime === -1 || cptime === 0) {
    return { ok: false, err: 400, msg: "Booked Time is not available" };
  }
  let totalDate = totalDateFromTo(
    new Date(),
    new Date(String(tour.booked_time))
  );
  if (totalDate < 2) {
    return { ok: false, err: 400, msg: "Booked Time is too close" };
  }

  const lastTrip = await carts.findOne({
    where: { user_id },
    order: [["updated_at", "DESC"]],
  });
  let newCart = 0;
  if (!lastTrip) {
    newCart = 1;
  } else {
    const findLastTour = await tours.findOne({
      where: { tour_id: lastTrip.tour_id, org_id: lastTrip.org_id },
    });
    let theEndOfLastTrip = newDateFromHours(
      new Date(String(lastTrip.booked_time)),
      findLastTour.hours
    );
    if (
      totalDateFromTo(theEndOfLastTrip, new Date(String(tour.booked_time))) < 2
    ) {
      return { ok: false, err: 400, msg: "The last trip is on going" };
    }
    newCart = lastTrip.cart_id + 1;
  }

  const finalForm = {
    cart_id: newCart,
    user_id: user_id,
    tour_id: tour.tour_id,
    org_id: tour.org_id,
    tour_name: findTour.tour_name,
    price: findTour.price,
    number_tourists: tour.number_tourists,
    booked_time: tour.booked_time,
    created_at: getNow(),
    updated_at: getNow(),
  };

  console.log(finalForm);

  let bookedTour = await carts.create(finalForm);
  return bookedTour;
};

const cancelBookedTour = async (user_id, cart_id) => {
  const findCartItem = await carts.findOne({
    where: { user_id, cart_id },
  });
  const copy = findCartItem;
  if (
    totalDateFromTo(new Date(), new Date(String(findCartItem.booked_time))) >=
    30
  ) {
    const isDelete = await findCartItem.destroy();
    console.log("Delete completed");
    console.log(isDelete);
    return { ok: true, msg: copy };
  }
  return { ok: false, msg: copy };
};

const getDetailOfBookedTour = async (user_id, cart_id) => {
  const findCartItem = await carts.findOne({ where: { cart_id, user_id } });
  if (!findCartItem) throw "DataIsNotExistedError";
  const tourInfo = await tours.findOne({
    attributes: ["tour_name", "hours", "number_days", "number_nights"],
    where: {
      tour_id: findCartItem.tour_id,
      org_id: findCartItem.org_id,
    },
    include: [
      {
        model: locations,
        as: "location",
        require: true,
        include: [{ model: countries, as: "country", require: true }],
      },
      {
        model: organizations,
        attributes: ["org_name", "address"],
        as: "org",
        require: true,
        include: [{ model: accounts, as: "account", require: true }],
      },
    ],
  });
  const returnData = {
    tour_name: tourInfo.tour_name,
    location_name: tourInfo.location.location_name,
    country_name: tourInfo.location.country.country_name,
    hours: tourInfo.hours,
    org_name: tourInfo.org.org_name,
    phone: tourInfo.org.account.phone,
    address: tourInfo.org.address,
    booked_time: findCartItem.booked_time,
    number_tourists: findCartItem.number_tourists,
    price: findCartItem.price,
  };
  return returnData;
};

//{ user_id, tour_id, org_id, cart_id }
const findCartItemsWithTour = async (condition) => {
  const listCarts = await carts.findAll({
    where: condition,
    include: [
      {
        model: tours,
        as: "tour",
        attributes: ["tour_name", "hours", "status_id"],
        require: true,
        on: {
          [Op.and]: [
            sequelize.where(
              sequelize.col("carts.tour_id"),
              "=",
              sequelize.col("tour.tour_id")
            ),
            sequelize.where(
              sequelize.col("carts.org_id"),
              "=",
              sequelize.col("tour.org_id")
            ),
          ],
        },
      },
    ],
  });
  return listCarts;
};

const findCartItem = async (cart_properties) => {
  const cart = await carts.findOne({ where: cart_properties });
  return cart;
};

const updateCompletedCartItem = async (cart_id, user_id) => {
  const findCartItem = await findCartItem({ cart_id, user_id });
  if (!findCartItem) throw "CartItemIsNotExistError";
  if (compareTime(new Date(findCartItem.booked_time), new Date()) !== 1)
    throw "TourOnGoingError";
  const isCompleted = findCartItem.completed;
  findCartItem.set({ completed: !isCompleted });
  const isSave = await findCartItem.save();
  return isSave;
};

const updateCompletedCartItemNoSave = async (cart_id, user_id) => {
  const findCartItem = await findCartItem({ cart_id, user_id });
  if (!findCartItem) throw "CartItemIsNotExistError";
  if (compareTime(new Date(findCartItem.booked_time), new Date()) !== 1)
    throw "TourOnGoingError";
  const isCompleted = findCartItem.completed;
  findCartItem.set({ completed: !isCompleted });
  // const isSave = await findCartItem.save();
};

const updateCompletedCartList = async (listCarts) => {
  let transactionList = [];
  try {
    for (let index = 0; index < listCarts.length; index++) {
      const item = listCarts[index];
      const unsavedItem = await updateCompletedCartItemNoSave(
        item.cart_id,
        item.user_id
      );
      transactionList.push(unsavedItem);
    }
  } catch (error) {
    console.log(error);
    switch (error.name) {
      case "CartItemIsNotExistError":
        return { ok: false, status: 404, message: "Cart Not Found" };
      case "TourOnGoingError":
        return {
          ok: false,
          status: 400,
          message: "Tour Is On Going. Can Not Mark Completed",
        };
      default:
        break;
    }
    return {ok: false, status: 500, message: "Unknown Error"}
  }
};

module.exports = {
  getCartOfUser,
  bookTour,
  cancelBookedTour,
  getDetailOfBookedTour,
  findCartItemsWithTour,
  findCartItem,
  updateCompletedCartItem,
};
