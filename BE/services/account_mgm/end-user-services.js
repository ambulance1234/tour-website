const commonErrors = require("../../application/errors/common-errors");

const users = require("../../config/model-instance").end_users;
const accounts = require("../../config/model-instance").accounts;

const getAllEndUsers = async () => {
  let allUsers = await users.findAll();
  return allUsers;
};

const getUserByName = async (username) => {
  let user = await users.findOne({
    where: {
      username: username,
    },
  });
  return user;
};

const getUserById = async (search) => {
  let user = await users.findOne({
    where: search,
    include: [
      {
        model: accounts,
        as: "account",
        attributes: ["email", "phone", "role_id", "country_id"],
        require: true,
      },
    ],
  });
  return user;
};

const register = async (account, userInfo) => {
  let tempUser = {
    account_id: account.account_id,
    username: userInfo.username,
    age: 17,
    address: "Unknown",
    fullname: "King of the Jungle",
    gender: "Unknown",
  };

  let findExist = await getUserByName(tempUser.username);
  if (findExist !== null) {
    throw "UserExistedError";
  }
  let newUser = await users.create(tempUser);
  let createdUser = {
    username: newUser.username,
    fullname: newUser.fullname,
    gender: newUser.gender,
    email: account.email,
    phone: account.phone,
    role_id: account.role_id,
    country_id: account.country_id,
  };
  return createdUser;
};

const updateUserInfo = async (account_id, newInfo) => {
  let findExist = await getUserByName(newInfo.username);

  if (findExist === null) {
    throw commonErrors.DataNotExistedError;
  }

  if (findExist.account_id !== account_id) {
    throw commonErrors.InformationNotMatchedError;
  }

  findExist.set(newInfo);
  let isSave = await findExist.save();
  return isSave;
};

const getReputationPoint = async (user_id) => {
  const repPoint = await users.findOne({
    attributes: ["rep_point"],
    where: {
      account_id: user_id,
    },
  });
  return repPoint;
};

const REPPOINTCASES = {
  TOURLEVEL1: "TOURLEVEL1",
  TOURLEVEL2: "TOURLEVEL2",
  TOURLEVEL3: "TOURLEVEL3",
  REVIEWONLYSTAR: "REVIEWONLYSTAR",
  REVIEWFULL: "REVIEWFULL",
  UPVOTE: "UPVOTE",
};

const REPPOINTACTION = {
  GIVEAWAY: "GIVEAWAY",
  TAKEBACK: "TAKEBACK"
}

const checkReceivedRepPoint = (RPCase) => {
  switch (RPCase) {
    //price of tour is lower than 1000
    case REPPOINTCASES.TOURLEVEL1:
      return 5;
    //price of tour is between 1000 and 5000
    case REPPOINTCASES.TOURLEVEL2:
      return 10;
    //price of tour is higher than 5000
    case REPPOINTCASES.TOURLEVEL3:
      return 20;
    //review has only star
    case REPPOINTCASES.REVIEWONLYSTAR:
      return 1;
    //review has full combo star and comment
    case REPPOINTCASES.REVIEWFULL:
      return 3;
    default:
      return 0;
  }
};

const updateReputaionPoint = async (user_id, RPCase, RPAction) => {
  const findExist = await getUserById(user_id);
  console.log(findExist);
  const curRepPoint = findExist.rep_point;
  let bonus = checkReceivedRepPoint(RPCase);
  if(RPAction === REPPOINTACTION.TAKEBACK) {
    bonus = 0 - bonus;
  }
  const result = Number(curRepPoint) + Number(bonus);
  console.log(
    `Current: ${curRepPoint}; Bonus: ${bonus}; New rep point: ${result}`
  );
  findExist.set({ rep_point: result });
  const isSave = await findExist.save();
  return isSave;
};

module.exports = {
  getAllEndUsers,
  register,
  getUserById,
  updateUserInfo,
  getReputationPoint,
  updateReputaionPoint,
  REPPOINTCASES,
  REPPOINTACTION
};
