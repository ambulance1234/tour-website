const md5 = require("md5");

// const env = require('../application/environment');
require("dotenv").config();
// const jwt = require("jsonwebtoken");
const commonErrors = require("../../application/errors/common-errors");
const time = require("../../application/time");
const accounts = require("../../config/model-instance").accounts;
const roles = require("../../config/model-instance").roles;
const countries = require("../../config/model-instance").countries;
const countryServices = require("../common/country-services");

const columns = [
  "account_id",
  "email",
  "password",
  "phone",
  "role_id",
  "country_id",
  "created_at",
];

const informationType = {
  email: 0,
  phone: 1,
  role: 2,
};

const getAll = async () => {
  let list = await accounts.findAll({
    attributes: columns,
    include: [
      {
        model: roles,
        as: "role",
        require: true,
      },
      {
        model: countries,
        as: "country",
        require: true,
      },
    ],
  });
  return list;
};

const verifyInformation = (info, infoType) => {
  if (typeof info !== "string") {
    return false;
  }
  if (informationType.email === infoType) {
    if (!info.includes("@")) {
      return false;
    }
  } else if (informationType.phone === infoType) {
    let pattern = /[^0-9]/g;
    if (info.match(pattern) === null) {
      return true;
    }
    return false;
  } else {
  }
};

const getByPhone = async (phone) => {
  let acc = await accounts.findOne({
    attributes: columns,
    where: {
      phone: phone,
    },
  });
  return acc;
};

const getByEmail = async (email) => {
  let acc = await accounts.findOne({
    attributes: columns,
    where: {
      email: email,
    },
  });
  return acc;
};

const gatherBaseInfo = (body) => {
  return {
    email: body.email,
    password: body.password,
    phone: body.phone,
    role: body.role,
    //role can be: USER, ORGANIZATION, ADMIN
    country: body.country,
  };
};

const register = async (account) => {
  if (
    !verifyInformation(account.email, informationType.email) &&
    !verifyInformation(account.phone, informationType.phone)
  ) {
    throw commonErrors.InvalidInformationError;
  }

  if (
    (await getByEmail(account.email)) !== null ||
    (await getByPhone(account.phone)) !== null
  ) {
    // return "Information has already existed!";
    throw commonErrors.DataExistedError;
  }

  //encode password
  account.password = md5(account.password);

  console.log(account.password);

  //verify ROLE
  console.log(typeof account.role);

  if (typeof account.role !== "string") {
    throw commonErrors.InvalidInformationError;
  }
  console.log(account.role);

  if (
    account.role === "USER" ||
    account.role === "ORGANIZATION" ||
    account.role === "ADMIN"
  ) {
  } else {
    // return "The role has not existed yet!";
    throw commonErrors.InvalidRoleError;
  }

  const roleId = await roles.findOne({
    attributes: ["role_id"],
    where: {
      role_name: account.role,
    },
  });

  //verify Country
  console.log(typeof account.country);

  if (typeof account.country !== "string") {
    throw commonErrors.InvalidInformationError;
  }

  let countryInfo = await countryServices.getByIdOrCode(
    undefined,
    account.country
  );

  console.log(countryInfo);

  // return "Some errors are occured while creating new Account";

  //create with sequelize
  let newAccount = await accounts.create(
    {
      email: account.email,
      phone: account.phone,
      password: account.password,
      role_id: roleId.role_id,
      country_id: countryInfo.country_id,
      created_at: time.getNow(),
    },
    {
      fields: [
        "email",
        "phone",
        "password",
        "role_id",
        "country_id",
        "created_at",
      ],
    }
  );
  return newAccount;
};

const deleteAccount = async (account) => {
  const deleted = await accounts.destroy({
    where: account
  });
  return deleted;
}

module.exports = {
  getAll,
  getByEmail,
  getByPhone,
  register,
  gatherBaseInfo,
  deleteAccount
};
