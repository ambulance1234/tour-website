const commonErrors = require("../../application/errors/common-errors");
const {
  totalDateFromTo,
  newDateFromHours,
  compareTime,
  getNow,
} = require("../../application/time");
const {
  reviews,
  end_users,
  carts,
  likes,
} = require("../../config/model-instance");
const cartService = require("../account_mgm/user-cart-services");
const endUserService = require("../account_mgm/end-user-services");

const getAvgStarOfTour = async (tour_id, org_id) => {
  const listReviews = await reviews.findAll({
    attributes: ["number_stars"],
    where: {
      tour_id,
      org_id,
    },
  });
  console.log(listReviews);
  let avg = 0;
  if (!listReviews || listReviews.length === 0) return 0;

  // avg = listReviews.reduce((prev, cur) => prev.number_stars + cur.number_stars, avg);
  listReviews.forEach((element) => {
    // console.log(element.number_stars);
    avg += element.number_stars;
  });
  avg /= listReviews.length;
  console.log("avg: ");
  console.log(avg);
  return avg === NaN ? 0 : avg.toFixed(1);
};

const getPreliminaryReviewsOfTour = async (tour_id, org_id) => {
  const avgStar = await getAvgStarOfTour(tour_id, org_id);
  // console.log(`avg Star: ${avgStar}`);
  const total = await getNumberReviewsOfTour(tour_id, org_id);
  return {
    average_star: avgStar,
    total_reviews: total,
  };
};

const getReviewsOfTour = async (tour_id, org_id, page = 1, limit = 3) => {
  const offset = (page - 1) * limit;
  const listReviews = await reviews.findAndCountAll({
    where: { tour_id, org_id },
    include: [
      { model: end_users, as: "user", attributes: ["username"], require: true },
    ],
    order: [["updated_at", "DESC"]],
    offset,
    limit,
  });
  return listReviews;
};

const getNumberReviewsOfTour = async (tour_id, org_id) => {
  const result = await reviews.count({ where: { tour_id, org_id } });
  return result;
};

const checkUpvoted = async (user_id, targetReview) => {
  const isLiked = await likes.findOne({
    where: {
      review_user_id: targetReview.user_id,
      review_tour_id: targetReview.tour_id,
      review_org_id: targetReview.org_id,
      upvoter_user_id: user_id,
    },
  });
  return isLiked;
};

// upvote
// check the reputation point of user which must be greater than 1
const upvoteReview = async (user_id, targetReview) => {
  const findReview = await reviews.findOne({
    where: {
      user_id: targetReview.user_id,
      tour_id: targetReview.tour_id,
      org_id: targetReview.org_id,
    },
  });
  if (!findReview) throw "ReviewNotExistError";
  if (await checkUpvoted(user_id, targetReview))
    throw "TheReviewHasBeenLikedError";
  const userInfo = await endUserService.getUserById({ account_id: user_id });
  if (userInfo.rep_point <= 0) {
    throw "RepPointZeroError";
  }
  if (!findReview.comment) throw "EmptyCommentReviewError";
  await likes.create({
    review_user_id: targetReview.user_id,
    reivew_tour_id: targetReview.tour_id,
    reivew_org_id: targetReview.org_id,
    upvoter_user_id: user_id,
  });
  const curUpvoted = findReview.upvote;
  findReview.set({ upvote: curUpvoted + 1 });
  const isUpvoted = await findReview.save();
  return isUpvoted;
};

// Check the tour cart of the user
// Make sure, user had the experience of that tour
// Date of booked tour must be older than now
// complete review, +1 reputation point of that user
// {
//   tour_id,
//   org_id,
//   number_stars,
//   comment
// }
const postNewReview = async (reviewPost, user_id) => {
  const listCarts = await cartService.findCartItemsWithTour({
    user_id,
    tour_id: reviewPost.tour_id,
    org_id: reviewPost.org_id,
  });
  if (!listCarts) throw "InexperiencedTourOrTourOnGoingError";
  let flag = false;
  listCarts.forEach((item) => {
    if (
      compareTime(
        newDateFromHours(new Date(item.booked_time), item.tour.hours),
        new Date()
      ) === -1
    ) {
      // if (item.completed === true) {
      //   console.log("found");
      //   return (flag = true);
      // }
      return flag = true;
    }
  });
  console.log(flag);
  if (flag === false) {
    throw "InexperiencedTourOrTourOnGoingError";
  }
  if (!reviewPost.number_stars) {
    throw "MissingStarsRatingError";
  }
  const findExistReview = await reviews.findOne({
    where: {
      user_id,
      tour_id: reviewPost.tour_id,
      org_id: reviewPost.org_id,
    },
  });
  if (findExistReview) throw "TourHasBeenReviewedError";
  const newReview = await reviews.create({
    user_id: user_id,
    tour_id: reviewPost.tour_id,
    org_id: reviewPost.org_id,
    number_stars: reviewPost.number_stars,
    comment: reviewPost.comment,
    created_at: getNow(),
    updated_at: getNow(),
  });
  return newReview;
};

module.exports = {
  getAvgStarOfTour,
  getReviewsOfTour,
  getNumberReviewsOfTour,
  getPreliminaryReviewsOfTour,
  postNewReview,
  upvoteReview,
};
