const commonErrors = require("../../application/errors/common-errors");
const {
  tours,
  locations,
  countries,
  organizations,
  available_status,
  reviews,
  carts,
  end_users,
  accounts,
} = require("../../config/model-instance");

const sequelize = require("sequelize");
const { getNow } = require("../../application/time");
const Op = sequelize.Op;

const tourShortAttributes = {
  attributes: [
    "tour_id",
    "org_id",
    "location_id",
    "status_id",
    "tour_name",
    "price",
  ],
};

//getAll
const getAllTours = async () => {
  const allTours = await tours.findAll({
    include: [
      {
        model: available_status,
        as: "status",
        attributes: ["status_name"],
        require: true,
      },
      {
        model: organizations,
        as: "org",
        attributes: ["org_name", "address"],
        require: true,
      },
      {
        model: locations,
        as: "location",
        attributes: ["location_name"],
        require: true,
        include: [
          {
            model: countries,
            as: "country",
            attributes: ["country_code", "country_name"],
            require: true,
          },
        ],
      },
    ],
  });
  return allTours;
};

//getByID
const getSpecifyTour = async (tour_id, org_id) => {
  const specifyTour = await tours.findOne({
    where: {
      tour_id,
      org_id,
      status_id: 1,
    },
    include: [
      {
        model: available_status,
        as: "status",
        attributes: ["status_name"],
        require: true,
      },
      {
        model: organizations,
        as: "org",
        attributes: ["org_name", "address"],
        require: true,
      },
      {
        model: locations,
        as: "location",
        attributes: ["location_name"],
        require: true,
        include: [
          {
            model: countries,
            as: "country",
            attributes: ["country_code", "country_name"],
            require: true,
          },
        ],
      },
    ],
  });
  return specifyTour;
};

//getByLocation
const getToursByLocation = async (location_name, page = 1) => {
  const limit = 10;
  const offset = (Number(page) - 1) * limit;
  const location = await locations.findOne({
    where: { location_name: location_name },
  });
  console.log(location);
  if (!location) throw commonErrors.DataNotExistedError;
  const listTours = await tours.findAndCountAll({
    attributes: [
      "tour_id",
      "org_id",
      "location_id",
      "status_id",
      "tour_name",
      "price",
      "introduce",
      "hours",
    ],
    where: {
      location_id: location.location_id,
      status_id: 1,
    },
    order: [["price", "ASC"]],
    offset,
    limit,
  });
  return listTours;
};
//get tours by company/orgazation
const getByOrganizationPagination = async (org_id, page = 1) => {
  const limit = 10;
  const offset = (Number(page) - 1) * limit;
  const listTours = await tours.findAll({
    attributes: ["tour_id", "location_id", "status_id", "tour_name", "price"],
    where: {
      org_id: org_id,
    },
    include: [{ model: locations, as: "location", require: true }],
    order: [["tour_id", "DESC"]],
    // offset,
    // limit,
  });
  return listTours;
};

const getTopRatingTours = async () => {
  const calcuAvgStar = sequelize.fn("AVG", sequelize.col("number_stars"));
  const roundAvgAtOneDigi = sequelize.fn("ROUND", calcuAvgStar, 1);
  const countReviewer = sequelize.fn("COUNT", sequelize.col("user_id"));
  const topListTours = await reviews.findAll({
    attributes: [
      "tour_id",
      "org_id",
      [roundAvgAtOneDigi, "avg_star"],
      [countReviewer, "reviewer"],
    ],
    order: [[roundAvgAtOneDigi, "DESC"]],
    group: ["tour_id", "org_id"],
    limit: 8,
    include: [
      {
        model: tours,
        as: "tour",
        attributes: tourShortAttributes.attributes,
        where: {
          status_id: 1,
        },
        require: true,
        on: {
          [Op.and]: [
            sequelize.where(
              sequelize.col("reviews.tour_id"),
              "=",
              sequelize.col("tour.tour_id")
            ),
            sequelize.where(
              sequelize.col("reviews.org_id"),
              "=",
              sequelize.col("tour.org_id")
            ),
          ],
        },
      },
    ],
  });
  console.log(topListTours);
  return topListTours;
};

const searchToursByIncludeName = async (tour_name, limit = 8, page = 1) => {
  const offset = (page - 1) * limit;
  const listTours = await tours.findAndCountAll({
    where: {
      tour_name: {
        [Op.like]: `%${tour_name}%`,
      },
      status_id: 1,
    },
    attributes: tourShortAttributes.attributes,
    limit,
    offset,
  });
  return listTours;
};

//create new tour
// {
//   org_id, => get from payload header
//   status_id,
//   location_name, [x]
//   tour_name,
//   price,
//   number_days, [x]
//   number_nights, [x]
//   hours, [x]
//   introduce,
//   description
// }
const createNewTour = async (tourPost) => {
  //check location
  const location = await locations.findOne({
    where: { location_name: tourPost.location_name },
  });
  console.log(location);
  if (!location) throw "LocationNotExistError";
  //check tour hours and number days and nights
  if (!tourPost.hours) {
    if (!tourPost.number_days || !tourPost.number_nights) {
      throw "MissingHoursOrNDNNError";
    }
    if (tourPost.number_nights > tourPost.number_days) {
      throw "NNIsGreatError";
    }
    tourPost.hours =
      (Number(tourPost.number_days) + Number(tourPost.number_nights)) * 12;
  }
  //get last tour of org
  const lastTourOfOrg = await tours.findOne({
    where: { org_id: tourPost.org_id },
    order: [["tour_id", "DESC"]],
  });
  let lastTourId = 0;
  if (!lastTourOfOrg) lastTourId = 1;
  else lastTourId = lastTourOfOrg.tour_id;
  const finalForm = {
    tour_id: Number(lastTourId) + 1,
    org_id: tourPost.org_id,
    location_id: location.location_id,
    tour_name: tourPost.tour_name,
    status_id: 1,
    price: tourPost.price,
    number_days: tourPost.number_days,
    number_nights: tourPost.number_nights,
    hours: tourPost.hours,
    introduce: tourPost.introduce,
    description: tourPost.description,
    created_at: getNow(),
  };
  console.log(finalForm);
  //create
  const createdTour = await tours.create(finalForm);
  return createdTour;
};
//update old tour
/*
{
  tour_id,
  org_id,
  ...
}
 */
const updateTourPost = async (newTourInfo) => {
  const findTour = await tours.findOne({
    where: {
      tour_id: newTourInfo.tour_id,
      org_id: newTourInfo.org_id,
    },
  });
  if (!findTour) throw "TourIsNotExistedError";
  findTour.set(newTourInfo);
  const isUpdated = await findTour.save();
  return isUpdated;
};

const disableMultiToursOfOrg = async (org, listTours) => {
  const findAndUpdateTours = await tours.update(
    { status_id: 3 },
    {
      where: {
        org_id: org,
        tour_id: {
          [Op.in]: listTours,
        },
      },
    }
  );
  return findAndUpdateTours;
};

const getListOfNumberOrderersOfTheTour = async (
  tour_id,
  org_id,
  page = 1,
  limit = 10
) => {
  const countCartId = sequelize.fn("COUNT", sequelize.col("cart_id"));
  const sumTourist = sequelize.fn("SUM", sequelize.col("number_tourists"));

  const offset = (page - 1) * limit;

  const count = await carts.count({
    where: { tour_id, org_id },
    group: ["booked_time"],
  });

  const list = await carts.findAll({
    attributes: [[sumTourist, "orderers"], "booked_time"],
    where: { tour_id, org_id },
    group: ["booked_time"],
    order: [["booked_time", "ASC"]],
    offset,
    limit,
  });
  const tour = await getSpecifyTour(tour_id, org_id);
  return { count: count.length, tour, list };
};
const getListOfTouristOfTourByDate = async (
  tour_id,
  org_id,
  booked_time,
  page = 1,
  limit = 10
) => {
  // log
  const offset = (page - 1) * limit;
  const time = new Date(String(booked_time));
  const listOrderers = await carts.findAndCountAll({
    where: {
      tour_id,
      org_id,
      booked_time: {
        [Op.like]: `%${time}%`,
      },
    },
    include: [
      {
        model: end_users,
        as: "user",
        attributes: ["account_id", "username", "fullname", "address"],
        require: true,
        include: [
          {
            model: accounts,
            as: "account",
            attributes: ["email", "phone"],
            require: true,
          },
        ],
      },
    ],
    offset,
    limit,
  });
  return listOrderers;
};

const deleteTours = async (org, listTours) => {
  console.log(
    "🚀 ~ file: tour-services.js ~ line 373 ~ deleteTours ~ listTours",
    listTours
  );
  const findAndUpdateTours = await tours.destroy({
    where: {
      org_id: org,
      tour_id: {
        [Op.in]: listTours,
      },
    },
  });
  return findAndUpdateTours;
};

module.exports = {
  getAllTours,
  getByOrganizationPagination,
  getSpecifyTour,
  getToursByLocation,
  createNewTour,
  getTopRatingTours,
  searchToursByIncludeName,
  updateTourPost,
  getListOfNumberOrderersOfTheTour,
  getListOfTouristOfTourByDate,
  disableMultiToursOfOrg,
  deleteTours,
};
