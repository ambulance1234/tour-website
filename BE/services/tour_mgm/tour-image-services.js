const formidable = require("formidable");
const bluebird = require("bluebird");
const fs = bluebird.promisifyAll(require("fs"));
const path = require("path");

const rootPath = path.dirname(path.dirname(__dirname));

const getTourImage = async (idTour, idOrg) => {
  const name = `tour-${idTour}-org-${idOrg}`;
  console.log(name);
  // console.log(__dirname);
  console.log(rootPath);
  const uploadsDirpath = rootPath + `/uploads/tours`;
  const allFiles = await fs.readdirAsync(uploadsDirpath);
  allFiles.forEach((item) => console.log(item));
  const fileBaseName = allFiles.find((item) => item.includes(name));
  console.log(fileBaseName);
  const imagePath = rootPath + `/uploads/tours/${fileBaseName}`;
  return imagePath;
};

const removeTourImage = async (idTour, idOrg) => {
  const name = `tour-${idTour}-org-${idOrg}`;
  console.log(name);
  console.log(__dirname);
  const uploadsDirpath = rootPath + `/uploads/tours`;
  const allFiles = await fs.readdirAsync(uploadsDirpath);
  allFiles.forEach((item) => console.log(item));
  const fileBaseName = allFiles.find((item) => item.includes(name));
  console.log(fileBaseName);
  const image = rootPath + `/uploads/tours/${fileBaseName}`;
  try {
    await fs.unlinkAsync(image);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

// Returns true if successful or false otherwise
async function checkCreateUploadsFolder(uploadsFolder) {
  try {
    await fs.statAsync(uploadsFolder);
  } catch (e) {
    if (e && e.code == "ENOENT") {
      console.log("The uploads folder doesn't exist, creating a new one...");
      try {
        await fs.mkdirAsync(uploadsFolder);
      } catch (err) {
        console.log("Error creating the uploads folder 1");
        return false;
      }
    } else {
      console.log("Error creating the uploads folder 2");
      return false;
    }
  }
  return true;
}

// Returns true or false depending on whether the file is an accepted type
function checkAcceptedExtensions(file) {
  console.log(file.mimetype);
  const type = file.mimetype.split("/").pop();
  const accepted = ["jpeg", "jpg", "png", "gif", "pdf"];
  if (accepted.indexOf(type) == -1) {
    return false;
  }
  return type;
}

//=============================================================================

const uploadTourImage = async (req, res) => {
  let form = formidable();
  const uploadsFolder = path.join(rootPath, "uploads/tours");
  console.log(`upload dir: ${uploadsFolder}`);
  form.multiples = true;
  form.uploadDir = uploadsFolder;
  form.maxFileSize = 5 * 1024 * 1024; // 5 MB
  const folderCreationResult = await checkCreateUploadsFolder(uploadsFolder);
  if (!folderCreationResult) {
    return {
      ok: false,
      err: 500,
      msg: "The uploads folder couldn't be created",
    };
  }
  
  form.parse(req, async (err, fields, files) => {
    let myUploadedFiles = [];
    if (err) {
      console.log("Error parsing the incoming form");
      const result = {
        ok: false,
        err: 500,
        msg: "Error passing the incoming form",
      };
      return res.status(result.err).json({ message: result });
    }
    // console.log(fields);
    console.log("=============================");
    // console.log(files);
    console.log("=============================");
    // If we are sending only one file:
    if (!files.files.length) {
      if (!String(fields.name).includes("tour")) {
        return { ok: false, err: 400, msg: "Name of picture is not valid" };
      }
      const file = files.files;
      const type = checkAcceptedExtensions(file);
      if (!type) {
        console.log("The received file is not a valid type");
        const result = {
          ok: false,
          err: 400,
          msg: "The sent file is not a valid type",
        };
        return res.status(result.err).json({ message: result });
      }
      //   const fileName = encodeURIComponent(
      //     file.originalFilename.replace(/&. *;+/g, "-")
      //   );
      //   myUploadedFiles.push(fileName);
      const newFileName = `${fields.name}.${type}`;
      myUploadedFiles.push(newFileName);
      try {
        // console.log(`file old path: ${file.filepath}`);
        // await fs.renameAsync(file.filepath, path.join(uploadsFolder, fileName));
        await fs.renameAsync(
          file.filepath,
          path.join(uploadsFolder, newFileName)
        );
      } catch (e) {
        console.log("Error uploading the file");
        console.log(e);
        try {
          await fs.unlinkAsync(file.filepath);
        } catch (e) {}
        const result = { ok: false, err: 400, msg: "Error uploading the file" };
        return res.status(result.err).json({ message: result });
      }
    }
    //for many files
    else {
      for (let i = 0; i < files.files.length; i++) {
        const file = files.files[i];
        if (!checkAcceptedExtensions(file)) {
          console.log("The received file is not a valid type");
          return {
            ok: false,
            err: 400,
            msg: "The sent file is not a valid type",
          };
        }
        const fileName = encodeURIComponent(
          file.originalFilename.replace(/&. *;+/g, "-")
        );
        myUploadedFiles.push(fileName);
        try {
          await fs.renameAsync(
            file.filepath,
            path.join(uploadsFolder, fileName)
          );
        } catch (e) {
          console.log("Error uploading the file");
          try {
            await fs.unlinkAsync(file.filepath);
          } catch (e) {}
          const result = {
            ok: false,
            err: 500,
            msg: "Error uploading the file",
          };
          return res.status(result.err).json({ message: result });
        }
      }
    }
    const result = {
      ok: true,
      err: 200,
      msg: "Files uploaded succesfully!",
      files: myUploadedFiles,
    };
    return res.status(result.err).json({message: result});

  });
};

module.exports = {
  getTourImage,
  removeTourImage,
  uploadTourImage,
};
