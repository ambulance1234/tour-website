const StatusCode = require("../application/status-code");
const services = require("../services/common/country-services");

const getAll = async (req, res) => {
  let list = await services.getAll();
  if (list === null || list.length === 0 || list === []) {
    res
      .status(StatusCode.NoContent.status)
      .json({ message: StatusCode.NoContent.message });
  } else {
    res.status(StatusCode.Ok.status).json({ message: list });
  }
};

const getByIdOrCode = async (req, res) => {
  let queries = req.query;
  let country = await services.getByIdOrCode(queries.id, queries.code);
//   console.log(country);
//   console.log(typeof country);
  if (
    country === null ||
    typeof country === undefined
  ) {
    res
      .status(StatusCode.NotFound.status)
      .json({ message: StatusCode.NotFound.message });
  } else {
    res.status(StatusCode.Ok.status).json({ message: country });
  }
};

module.exports = { getAll, getByIdOrCode };
