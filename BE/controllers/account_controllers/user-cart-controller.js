const cartService = require("../../services/account_mgm/user-cart-services");
const endUserService = require("../../services/account_mgm/end-user-services");
const StatusCode = require("../../application/status-code");

//book tour
//call book tour in cart service first
//then call update reputaion point later
const bookTheTourFromUser = async (req, res) => {
  const body = req.body;
  const userInfo = req.user;
  try {
    const bookedTour = await cartService.bookTour(userInfo.account_id, body);
    if (bookedTour.ok !== undefined && bookedTour.ok === false) {
      return res.status(bookedTour.err).json({ message: bookedTour.msg });
    }
    const updateRepPoint = await endUserService.updateReputaionPoint(
      userInfo.account_id,
      checkTourPrice(bookedTour.price),
      endUserService.REPPOINTACTION.GIVEAWAY
    );
    return res
      .status(StatusCode.Created.status)
      .json({ message: { bookedTour, updateRepPoint } });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const checkTourPrice = (price) => {
  if (price < 1000) {
    return endUserService.REPPOINTCASES.TOURLEVEL1;
  } else if (price >= 1000 && price < 5000) {
    return endUserService.REPPOINTCASES.TOURLEVEL2;
  } else {
    return endUserService.REPPOINTCASES.TOURLEVEL3;
  }
};

const getCartOfUser = async (req, res) => {
  const userInfo = req.user;
  const query = req.query;
  try {
    const cartInfo = await cartService.getCartOfUser(
      userInfo.account_id,
      query.page,
      query.limit
    );
    return res.status(StatusCode.Ok.status).json({ message: cartInfo });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const cancelBookedTour = async (req, res) => {
  const userInfo = req.user;
  const query = req.query;
  try {
    const cartInfo = await cartService.cancelBookedTour(
      userInfo.account_id,
      query.cart
    );
    if (cartInfo.ok === true) {
      const findUser = await endUserService.updateReputaionPoint(
        userInfo.account_id,
        checkTourPrice(cartInfo.msg.price),
        endUserService.REPPOINTACTION.TAKEBACK
      );
      return res.status(StatusCode.Ok.status).json({
        message: {
          note: "Cancelled Completely Booked Tour",
          cart: cartInfo.msg,
          user: findUser,
        },
      });
    } else {
      return res.status(StatusCode.BadRequest.status).json({
        message: {
          note: "Can not cancel Booked Tour Because Under 30 days",
          cart: cartInfo.msg,
        },
      });
    }
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const getDetailOfBookedTour = async (req, res) => {
  const query = req.query;
  const userInfo = req.user;
  try {
    const detail = await cartService.getDetailOfBookedTour(
      userInfo.account_id,
      query.cart
    );
    return res.status(200).json({ message: detail });
  } catch (error) {
    console.log(error);
    if (error === "DataIsNotExistedError")
      return res.status(404).json({ message: "Data Not Existed" });
    return res.sendStatus(500);
  }
};

const updateCompletedCart = async (req, res) => {
  const body = req.body;
  try {
    const isUpdated = await cartService.updateCompletedCartItem(
      body.cart_id,
      body.user_id
    );
    return res.status(200).json({ message: isUpdated });
  } catch (error) {
    console.log(error);
    switch (error) {
      case "CartItemIsNotExistError":
        return res.status(404).json({ message: "Cart Not Found" });
        case "TourOnGoingError":
        return res.status(400).json({ message: "Tour Is On Going. Can Not Mark Completed" });
      default:
        break;
    }
    return res.sendStatus(500);
  }
};

module.exports = {
  bookTheTourFromUser,
  getCartOfUser,
  cancelBookedTour,
  getDetailOfBookedTour,
  updateCompletedCart
};
