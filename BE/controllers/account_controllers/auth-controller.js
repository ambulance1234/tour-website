const services = require("../../services/account_mgm/auth-services");
const StatusCode = require("../../application/status-code");
const login = async (req, res) => {
  let information = req.body;
  let user = await services.login(information);
  if (typeof user === "boolean" && user === false) {
    res.status(StatusCode.BadRequest.status).json({ message: user });
  } else if (typeof user === "string") {
    res.status(StatusCode.NotFound.status).json({ message: user });
  } else {
    res.status(StatusCode.Ok.status).json({ message: user });
  }
};
const logout = async (req, res) => {
  const userToken = services.logout(req.user);
  res.status(200).json({ message: userToken });
};
module.exports = {
  login,
  logout,
};
