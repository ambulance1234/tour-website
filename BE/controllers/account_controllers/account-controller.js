const services = require("../../services/account_mgm/account-services");
const errorHandler = require("../../application/errors/errors-handle");
const StatusCode = require("../../application/status-code");

const getAll = async (req, res) => {
  let list = await services.getAll();
  if (list === null || list.length === 0 || list === []) {
    res
      .status(StatusCode.NoContent.status)
      .json({ message: StatusCode.NoContent.message });
  } else {
    res.status(StatusCode.Ok.status).json({ message: list });
  }
};

const register = async (req, res) => {
  let body = req.body;
  console.log(body);
  if (typeof body !== "object") {
    res
      .status(StatusCode.UnsupportedMediaType.status)
      .json(StatusCode.UnsupportedMediaType.message);
  }
  try {
    let newAcc = await services.register(body);
    res.status(StatusCode.Created.status).json({ massage: newAcc });
  } catch (error) {
    return errorHandler.registerErrorsCatched(res, error);
  }
};

module.exports = { getAll, register };
