const adminService = require("../../services/account_mgm/admin-services");
const accountBaseServices = require("../../services/account_mgm/account-services");
const errorHandler = require("../../application/errors/errors-handle");

const getAllAdmins = async (req, res) => {
  try {
    const listAdmins = await adminService.getAllAdmins();
    return res.status(200).json({message: listAdmins});
  } catch (error) {
    console.log(error);
    return res.sendStatus(500)
  }
}

const adminRegister = async (req, res) => {
  let body = req.body;
  if (body === null) {
    return res.sendStatus(StatusCode.BadRequest.status);
  }
  body.role = "ADMIN";
  let baseInfo = accountBaseServices.gatherBaseInfo(body);
  let userInfo = {
    username: body.username,
    secret_key: body.secret_key
  };
  try {
    let account = await accountBaseServices.register(baseInfo);
    let newUser = await adminService.register(account, userInfo);
    return res.status(StatusCode.Created.status).json({ message: newUser });
  } catch (error) {
    console.log("error", error);
    return errorHandler.registerErrorsCatched(res, error);
  }
}

const deleteEndUserAction = async (req, res) => {
  const body = req.body;
  const adminInfo = req.user;
  try {
    await adminService.deleteUserAction(
      { account_id: adminInfo.account_id, secret_key: body.secret_key },
      { account_id: body.user_id }, "USER"
    );
    res.status(200).json({ message: "Delete Completed" });
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};

const deleteOrgAction = async (req, res) => {
    const body = req.body;
    const adminInfo = req.user;
    try {
      await adminService.deleteUserAction(
        { account_id: adminInfo.account_id, secret_key: body.secret_key },
        { account_id: body.user_id }, "ORG"
      );
      res.status(200).json({ message: "Delete Completed" });
    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }
  };

module.exports = {
  getAllAdmins,
  deleteEndUserAction,
  deleteOrgAction,
  adminRegister
};
