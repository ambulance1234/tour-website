const orgServices = require("../../services/account_mgm/org-services");
const errorHandle = require("../../application/errors/errors-handle");
const commonErrors = require("../../application/errors/common-errors");
const accountBaseServices = require("../../services/account_mgm/account-services");
const StatusCode = require("../../application/status-code");

const getAllOrgs = async (req, res) => {
  try {
    let allOrgs = await orgServices.getAllOrgs();
    res.status(StatusCode.Ok.status).json({ message: allOrgs });
  } catch (error) {
    console.log(error);
    res.sendStatus(StatusCode.ServerError.status);
  }
};

const getOrgByIdOrBLicense = async (req, res) => {
  let searchItem = req.body;
  if (!searchItem)
    res.status(StatusCode.BadRequest.status).json({ message: "Missing body" });
  try {
    let allOrgs = await orgServices.getOrgByIdOrBLicense(searchItem);
    if (allOrgs === null) {
      return res.sendStatus(StatusCode.NotFound.status);
    }
    res.status(StatusCode.Ok.status).json({ message: allOrgs });
  } catch (error) {
    console.log(error);
    res.sendStatus(StatusCode.ServerError.status);
  }
};

const orgRegister = async (req, res) => {
  let body = req.body;
  if (body === null) {
    return res.sendStatus(StatusCode.BadRequest.status);
  }
  body.role = "ORGANIZATION";
  let baseInfo = accountBaseServices.gatherBaseInfo(body);
  let orgInfo = {
    org_name: body.org_name,
    address: body.address,
    business_license: body.business_license,
    confirm_code: body.confirm_code,
  };
  try {
    let account = await accountBaseServices.register(baseInfo);
    let newOrg = await orgServices.orgRegister(account, orgInfo);
    return res.status(StatusCode.Created.status).json({ message: newOrg });
  } catch (error) {
    console.log(error);
    if (error === "OrgExistedError") {
      const deleted = await accountBaseServices.deleteAccount({
        email: body.email,
      });
      console.log(deleted);
      if (deleted != 0) {
        return res.status(409).json({ message: "Organization Existed" });
      }
      else {
        return res.sendStatus(500);
      }
    }
    return errorHandle.registerErrorsCatched(res, error);
  }
};
const updateOrgInfo = async (req, res) => {
  let body = req.body;
  if (!body) {
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing body" });
  }
  if (!body.org_name || !body.confirm_code) {
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing info" });
  }
  let authorizedInfo = req.user;
  try {
    let updatedInfo = await orgServices.updateOrgInfo(
      authorizedInfo.account_id,
      body
    );
    return res.status(StatusCode.Ok.status).json({ message: updatedInfo });
  } catch (error) {
    return errorHandle.updatedErrorCatched(res, error);
  }
};

const changeConfirmCode = async (req, res) => {
  let body = req.body;
  if (!body) {
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing body" });
  }
  if (!body.oldCCode || !body.newCCode || !body.password) {
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing Info" });
  }
  let authorizedInfo = req.user;
  try {
    let updatedCode = await orgServices.changeConfirmCode({
      account_id: authorizedInfo.account_id,
      oldCCode: body.oldCCode,
      newCCode: body.newCCode,
      password: body.password,
    });
    return res.status(StatusCode.Ok.status).json({ message: updatedCode });
  } catch (error) {
    console.log(error);
    return errorHandle.updatedErrorCatched(res, error);
  }
};

const searchOrgs = async (req, res) => {
  let body = req.body;
  if (!body) {
    return res
      .sendStatus(StatusCode.BadRequest.status)
      .json({ message: "Missing body" });
  }
  console.log(body);
  try {
    let orgs = await orgServices.searchOrgs(body);
    if (!orgs || orgs.length === 0)
      return res.sendStatus(StatusCode.NotFound.status);
    return res.status(StatusCode.Ok.status).json({ message: orgs });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

module.exports = {
  getAllOrgs,
  getOrgByIdOrBLicense,
  orgRegister,
  updateOrgInfo,
  changeConfirmCode,
  searchOrgs,
};
