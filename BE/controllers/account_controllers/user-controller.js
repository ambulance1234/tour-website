const endUserServices = require("../../services/account_mgm/end-user-services");
const errorHandler = require("../../application/errors/errors-handle");
const StatusCode = require("../../application/status-code");
const accountBaseServices = require("../../services/account_mgm/account-services");
const commonErrors = require("../../application/errors/common-errors");

const getAllEndUsers = async (req, res) => {
  try {
    let allUsers = await endUserServices.getAllEndUsers();
    res.status(StatusCode.Ok.status).json({ message: allUsers });
  } catch (error) {
    console.log(error);
    res.sendStatus(StatusCode.ServerError.status);
  }
};

const getEndUserByIdOrName = async (req, res) => {
  let searchQuery = req.query;
  try {
    let allUsers = await endUserServices.getUserById(searchQuery);
    if (allUsers === null) {
      return res.sendStatus(StatusCode.NotFound.status);
    }
    res.status(StatusCode.Ok.status).json({ message: allUsers });
  } catch (error) {
    console.log(error);
    res.sendStatus(StatusCode.ServerError.status);
  }
};

const userRegister = async (req, res) => {
  let body = req.body;
  if (body === null) {
    return res.sendStatus(StatusCode.BadRequest.status);
  }
  body.role = "USER";
  let baseInfo = accountBaseServices.gatherBaseInfo(body);
  let userInfo = {
    username: body.username,
  };
  try {
    let account = await accountBaseServices.register(baseInfo);
    let newUser = await endUserServices.register(account, userInfo);
    return res.status(StatusCode.Created.status).json({ message: newUser });
  } catch (error) {
    console.log("error", error);
    if (error === "UserExistedError") {
      const deleted = await accountBaseServices.deleteAccount({
        email: body.email,
      });
      console.log(deleted);
      if (deleted != 0) {
        return res.status(409).json({ message: "User Existed" });
      }
      else {
        return res.sendStatus(500);
      }
    }
    return errorHandler.registerErrorsCatched(res, error);
  }
};

const updateUserInfo = async (req, res) => {
  let body = req.body;
  if (!body)
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing body" });

  if (!body.username)
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing username" });

  //info from authorization filter
  let authorizedInfo = req.user;
  try {
    let updatedInfo = await endUserServices.updateUserInfo(
      authorizedInfo.account_id,
      body
    );
    res.status(StatusCode.Ok.status).json({ message: updatedInfo });
  } catch (error) {
    console.log("error", error);
    return errorHandler.updatedErrorCatched(res, error);
  }
};

module.exports = {
  getAllEndUsers,
  getEndUserByIdOrName,
  userRegister,
  updateUserInfo,
};
