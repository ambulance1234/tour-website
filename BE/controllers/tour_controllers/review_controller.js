const reviewService = require("../../services/tour_mgm/review-services");
const endUserService = require("../../services/account_mgm/end-user-services");

const getReviewsOfTour = async (req, res) => {
  const query = req.query;
  try {
    const listReviews = await reviewService.getReviewsOfTour(
      query.tour,
      query.org,
      query.page,
      query.limit
    );
    return res.status(200).json({ message: listReviews });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const getNumberReviewsOfTour = async (req, res) => {
  const query = req.query;
  try {
    const result = await reviewService.getNumberReviewsOfTour(
      query.tour,
      query.org
    );
    return res.status(200).json({ message: result });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const postNewReview = async (req, res) => {
  const body = req.body;
  const userInfo = req.user;
  try {
    const newReview = await reviewService.postNewReview(
      body,
      userInfo.account_id
    );
    let reviewCase = endUserService.REPPOINTCASES.REVIEWFULL;
    if (!newReview.comment) {
      reviewCase = endUserService.REPPOINTCASES.REVIEWONLYSTAR;
    }
    const updateUserRPoint = await endUserService.updateReputaionPoint(
      userInfo.account_id,
      reviewCase,
      endUserService.REPPOINTACTION.GIVEAWAY
    );
    return res
      .status(201)
      .json({ message: { review: newReview, user: updateUserRPoint } });
  } catch (error) {
    console.log(error);
    switch (error) {
      case "InexperiencedTourOrTourOnGoingError":
        return res
          .status(400)
          .json({ message: "Tour has not been experienced or on going" });
      case "MissingStarsRatingError":
        return res.status(400).json({ message: "Review missing stars rating" });
      case "TourHasBeenReviewedError":
        return res
          .status(400)
          .json({ message: "Review has been post for this tour" });
      default:
        break;
    }
    res.sendStatus(500);
  }
};

const upvoteReview = async (req, res) => {
  const query = req.query;
  const userInfo = req.user;
  try {
    const isUpvoted = await reviewService.upvoteReview(userInfo.account_id, {
      tour_id: query.tour,
      org_id: query.org,
      user_id: query.user,
    });
    return res.status(200).json({ message: isUpvoted });
  } catch (error) {
    console.log(error);
    switch (error) {
      case "ReviewNotExistError":
        return res.status(404).json({ message: "Review Not Found" });
      case "TheReviewHasBeenLikedError":
        return res.status(400).json({ message: "User Has Liked This Review" });
      case "RepPointZeroError":
        return res.status(400).json({ message: "Reputation Of User Is Zero" });
      case "EmptyCommentReviewError":
        return res
          .status(400)
          .json({ message: "The Review Has No Comment To Upvote" });
      default:
        break;
    }
    res.sendStatus(500);
  }
};

module.exports = {
  getReviewsOfTour,
  getNumberReviewsOfTour,
  postNewReview,
  upvoteReview,
};
