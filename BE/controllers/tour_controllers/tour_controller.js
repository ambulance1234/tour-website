const tourService = require("../../services/tour_mgm/tour-services");
const reviewService = require("../../services/tour_mgm/review-services");
const StatusCode = require("../../application/status-code");
const commonErrors = require("../../application/errors/common-errors");

const getDetailTour = async (req, res) => {
  const query = req.query;
  try {
    let listTours = await tourService.getSpecifyTour(query.tour, query.org);
    if (!listTours) return res.sendStatus(StatusCode.NotFound.status);

    const preliminary = await reviewService.getPreliminaryReviewsOfTour(
      query.tour,
      query.org
    );

    const result = {
      tours: listTours,
      // reviews: listReviews,
      preliminary: preliminary,
    };

    // listTours.reviews = listReviews;
    // listTours.avg = avgStars;
    console.log(result);
    return res.status(StatusCode.Ok.status).json({ message: result });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const searchToursWithLocation = async (req, res) => {
  const query = req.query;
  try {
    const listTours = await tourService.getToursByLocation(
      query.location_name,
      query.page === null ? 1 : query.page
    );
    if (Array.isArray(listTours) && listTours.length === 0)
      return res.sendStatus(StatusCode.NotFound.status);
    return res.status(StatusCode.Ok.status).json({ message: listTours });
  } catch (error) {
    console.log(error);
    if (error === commonErrors.DataNotExistedError)
      return res.sendStatus(StatusCode.NotFound.status);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const getToursOfOrg = async (req, res) => {
  const query = req.query;
  try {
    const listTours = await tourService.getByOrganizationPagination(
      query.org,
      query.page === null ? 1 : query.page
    );
    if (Array.isArray(listTours) && listTours.length === 0)
      return res.sendStatus(StatusCode.NotFound.status);

    return res.status(StatusCode.Ok.status).json({ message: listTours });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const getTopRaingTours = async (req, res) => {
  try {
    const topTours = await tourService.getTopRatingTours();
    res.status(StatusCode.Ok.status).json({ message: topTours });
  } catch (error) {
    console.log(error);
    res.sendStatus(StatusCode.ServerError.status);
  }
};

const searchTourByName = async (req, res) => {
  const query = req.query;
  try {
    const listTours = await tourService.searchToursByIncludeName(query.keyword);
    return res.status(StatusCode.Ok.status).json({ message: listTours });
  } catch (error) {
    console.log(error);
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const postNewTour = async (req, res) => {
  const body = req.body;
  const orgInfo = req.user;
  try {
    body.org_id = orgInfo.account_id;
    console.log("body of request after modifying \n");
    console.log(body);
    const posted = await tourService.createNewTour(body);
    // if (!posted.ok)
    //   return res.status(posted.status).json({ message: posted.msg });
    return res.status(StatusCode.Created.status).json({ message: posted });
  } catch (error) {
    console.log(error);
    switch (error) {
      case "LocationNotExistError":
        return res.status(400).json({
          message: "Location is not exist",
        });
      case "MissingHoursOrNDNNError":
        return res.status(400).json({
          message: "Missing hours or number days and number nights",
        });
      case "NNIsGreatError":
        return res.status(400).json({
          message: "Number Nights can not be greater than Number Days",
        });
      default:
        break;
    }
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const updateTourPost = async (req, res) => {
  const body = req.body;
  const orgInfo = req.user;
  body.org_id = orgInfo.account_id;
  try {
    const updatedTour = await tourService.updateTourPost(body);
    return res.status(200).json({ message: updatedTour });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const getListOfNumberOrderersOfTheTour = async (req, res) => {
  const query = req.query;
  const orgInfo = req.user;

  try {
    const list = await tourService.getListOfNumberOrderersOfTheTour(
      query.tour,
      orgInfo.account_id,
      query.page,
      query.limit
    );
    return res.status(200).json({ message: list });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const getListOfTouristOfTourByDate = async (req, res) => {
  const body = req.body;
  const query = req.query;
  const orgInfo = req.user;
  try {
    const list = await tourService.getListOfTouristOfTourByDate(
      query.tour,
      orgInfo.account_id,
      query.time,
      query.page,
      query.limit
    );
    return res.status(200).json({ message: list });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};
const disableMultiToursOfOrg = async (req, res) => {
  const orgInfo = req.user;
  const body = req.body;
  try {
    const list = await tourService.disableMultiToursOfOrg(
      orgInfo.account_id,
      body.tours
    );
    return res.status(200).json({ message: list });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const deleteTours = async (req, res) => {
  const body = req.body;
  const orgInfo = req.user;
  console.log(
    "🚀 ~ file: tour_controller.js ~ line 188 ~ deleteTours ~ orgInfo",
    orgInfo
  );
  console.log(
    "🚀 ~ file: tour_controller.js ~ line 187 ~ deleteTours ~ body",
    body
  );
  try {
    const deleted = await tourService.deleteTours(
      orgInfo.account_id,
      body.tours
    );
    return res.status(200).json({ message: deleted });
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};

module.exports = {
  getDetailTour,
  searchToursWithLocation,
  getToursOfOrg,
  getTopRaingTours,
  postNewTour,
  searchTourByName,
  updateTourPost,
  getListOfNumberOrderersOfTheTour,
  getListOfTouristOfTourByDate,
  disableMultiToursOfOrg,
  deleteTours,
};
