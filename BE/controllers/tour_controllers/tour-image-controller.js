const imageService = require("../../services/tour_mgm/tour-image-services");

const getImage = async (req, res) => {
  const query = req.query;
  try {
    const image = await imageService.getTourImage(query.tour, query.org);
    return res.sendFile(image);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const uploadImg = async (req, res) => {
  try {
    return await imageService.uploadTourImage(req, res);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const removeImg = async (req, res) => {
  const query = req.query;
  try {
    const image = await imageService.getTourImage(query.tour, query.org);
    return res.sendFile(image);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

module.exports = {
  getImage,
  uploadImg,
  removeImg
};
