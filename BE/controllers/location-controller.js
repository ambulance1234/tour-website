const locationServices = require("../services/common/location-services");
const StatusCode = require("../application/status-code");
const commonErrors = require("../application/errors/common-errors");

const getAll = async (req, res) => {
  const allLocations = await locationServices.getAll();
  if (!allLocations)
    return res.status(StatusCode.Ok.status).json({ message: [] });
  return res.status(StatusCode.Ok.status).json({ message: allLocations });
};

const getLocationsByName = async (req, res) => {
  const query = req.query;
  if (!query) return res.sendStatus(StatusCode.BadRequest.status);
  try {
    const location = await locationServices.getLocationByName(query.name);
    if (!location) return res.sendStatus(StatusCode.NotFound.status);
    return res.status(StatusCode.Ok.status).json({ message: location });
  } catch (error) {
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const getLocationsByCountryCode = async (req, res) => {
  const query = req.query;
  if (!query) return res.sendStatus(StatusCode.BadRequest.status);
  try {
    const locationsList = await locationServices.getByCountry(query.code);
    if (
      !locationsList ||
      (Array.isArray(locationsList) && locationsList.length === 0)
    ) {
      return res.sendStatus(StatusCode.NotFound.status);
    }
    return res.status(StatusCode.Ok.status).json({ message: locationsList });
  } catch (error) {
    return res.sendStatus(StatusCode.ServerError.status);
  }
};

const createNewLocation = async (req, res) => {
  const body = req.body;
  if (!body) {
    return res
      .status(StatusCode.BadRequest.status)
      .json({ message: "Missing body" });
  }
  try {
    const createdLocation = await locationServices.createNewLocation(body);
    return res
      .status(StatusCode.Created.status)
      .json({ message: createdLocation });
  } catch (error) {
    switch (error) {
      case commonErrors.InvalidInformationError:
        return res
          .status(StatusCode.BadRequest.status)
          .json({ message: "Missing name of location" });
      case commonErrors.DataExistedError:
        return res
          .status(StatusCode.BadRequest.status)
          .json({ message: "Location is Existed" });
      default:
        return res.sendStatus(StatusCode.ServerError.status);
    }
  }
};
module.exports = {
  getAll,
  getLocationsByCountryCode,
  getLocationsByName,
  createNewLocation,
};
