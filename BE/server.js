var express = require("express");
var cors = require("cors");
var app = express();

const port = 5000;

app.use(cors());

app.use(express.json())

app.get("/healthz", (req, res) => { 
  console.log("Server still alive"); 
  return res.status(200).json(); 
})

app.get("/welcome", (req, res) => {
  console.log("Welcome to Tour API Service");
  return res.status(200).json({
    message: "Welcome to Tour API Service", status: 200
  });
})

app.use("/api/countries", require("./routers/country-router"));

app.use("/api/accounts", require("./routers/account-router"));
app.use("/api/gateway", require("./routers/auth-router"));
app.use("/api/tours", require('./routers/tour-router'));
app.use("/api/admins", require('./routers/admin-router'));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
