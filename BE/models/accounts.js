const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "accounts",
    {
      account_id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: "idx_email_acc",
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: "idx_phone_acc",
      },
      role_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "roles",
          key: "role_id",
        },
      },
      country_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "countries",
          key: "country_id",
        },
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "accounts",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "account_id" }],
        },
        {
          name: "idx_email_acc",
          unique: true,
          using: "BTREE",
          fields: [{ name: "email" }],
        },
        {
          name: "idx_phone_acc",
          unique: true,
          using: "BTREE",
          fields: [{ name: "phone" }],
        },
        {
          name: "fk_acc_role",
          using: "BTREE",
          fields: [{ name: "role_id" }],
        },
        {
          name: "fk_acc_ctr",
          using: "BTREE",
          fields: [{ name: "country_id" }],
        },
      ],
    }
  );
};
