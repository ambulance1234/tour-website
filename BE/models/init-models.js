var DataTypes = require("sequelize").DataTypes;
var _account_avatars = require("./account_avatars");
var _accounts = require("./accounts");
var _admins = require("./admins");
var _available_status = require("./available_status");
var _carts = require("./carts");
var _countries = require("./countries");
var _end_users = require("./end_users");
var _likes = require("./likes");
var _locations = require("./locations");
var _organizations = require("./organizations");
var _report_tour_evidences = require("./report_tour_evidences");
var _report_tours = require("./report_tours");
var _reviews = require("./reviews");
var _roles = require("./roles");
var _tour_photos = require("./tour_photos");
var _tours = require("./tours");

function initModels(sequelize) {
  var account_avatars = _account_avatars(sequelize, DataTypes);
  var accounts = _accounts(sequelize, DataTypes);
  var admins = _admins(sequelize, DataTypes);
  var available_status = _available_status(sequelize, DataTypes);
  var carts = _carts(sequelize, DataTypes);
  var countries = _countries(sequelize, DataTypes);
  var end_users = _end_users(sequelize, DataTypes);
  var likes = _likes(sequelize, DataTypes);
  var locations = _locations(sequelize, DataTypes);
  var organizations = _organizations(sequelize, DataTypes);
  var report_tour_evidences = _report_tour_evidences(sequelize, DataTypes);
  var report_tours = _report_tours(sequelize, DataTypes);
  var reviews = _reviews(sequelize, DataTypes);
  var roles = _roles(sequelize, DataTypes);
  var tour_photos = _tour_photos(sequelize, DataTypes);
  var tours = _tours(sequelize, DataTypes);

  account_avatars.belongsTo(accounts, { as: "account", foreignKey: "account_id"});
  accounts.hasOne(account_avatars, { as: "account_avatar", foreignKey: "account_id"});
  admins.belongsTo(accounts, { as: "account", foreignKey: "account_id"});
  accounts.hasOne(admins, { as: "admin", foreignKey: "account_id"});
  end_users.belongsTo(accounts, { as: "account", foreignKey: "account_id"});
  accounts.hasOne(end_users, { as: "end_user", foreignKey: "account_id"});
  organizations.belongsTo(accounts, { as: "account", foreignKey: "account_id"});
  accounts.hasOne(organizations, { as: "organization", foreignKey: "account_id"});
  tours.belongsTo(available_status, { as: "status", foreignKey: "status_id"});
  available_status.hasMany(tours, { as: "tours", foreignKey: "status_id"});
  accounts.belongsTo(countries, { as: "country", foreignKey: "country_id"});
  countries.hasMany(accounts, { as: "accounts", foreignKey: "country_id"});
  locations.belongsTo(countries, { as: "country", foreignKey: "country_id"});
  countries.hasMany(locations, { as: "locations", foreignKey: "country_id"});
  carts.belongsTo(end_users, { as: "user", foreignKey: "user_id"});
  end_users.hasMany(carts, { as: "carts", foreignKey: "user_id"});
  likes.belongsTo(end_users, { as: "upvoter_user", foreignKey: "upvoter_user_id"});
  end_users.hasMany(likes, { as: "likes", foreignKey: "upvoter_user_id"});
  report_tours.belongsTo(end_users, { as: "user", foreignKey: "user_id"});
  end_users.hasMany(report_tours, { as: "report_tours", foreignKey: "user_id"});
  reviews.belongsTo(end_users, { as: "user", foreignKey: "user_id"});
  end_users.hasMany(reviews, { as: "reviews", foreignKey: "user_id"});
  tours.belongsTo(locations, { as: "location", foreignKey: "location_id"});
  locations.hasMany(tours, { as: "tours", foreignKey: "location_id"});
  tours.belongsTo(organizations, { as: "org", foreignKey: "org_id"});
  organizations.hasMany(tours, { as: "tours", foreignKey: "org_id"});
  report_tour_evidences.belongsTo(report_tours, { as: "user", foreignKey: "user_id"});
  report_tours.hasMany(report_tour_evidences, { as: "report_tour_evidences", foreignKey: "user_id"});
  report_tour_evidences.belongsTo(report_tours, { as: "tour", foreignKey: "tour_id"});
  report_tours.hasMany(report_tour_evidences, { as: "tour_report_tour_evidences", foreignKey: "tour_id"});
  report_tour_evidences.belongsTo(report_tours, { as: "org", foreignKey: "org_id"});
  report_tours.hasMany(report_tour_evidences, { as: "org_report_tour_evidences", foreignKey: "org_id"});
  likes.belongsTo(reviews, { as: "review_user", foreignKey: "review_user_id"});
  reviews.hasMany(likes, { as: "likes", foreignKey: "review_user_id"});
  likes.belongsTo(reviews, { as: "review_tour", foreignKey: "review_tour_id"});
  reviews.hasMany(likes, { as: "review_tour_likes", foreignKey: "review_tour_id"});
  likes.belongsTo(reviews, { as: "review_org", foreignKey: "review_org_id"});
  reviews.hasMany(likes, { as: "review_org_likes", foreignKey: "review_org_id"});
  accounts.belongsTo(roles, { as: "role", foreignKey: "role_id"});
  roles.hasMany(accounts, { as: "accounts", foreignKey: "role_id"});
  carts.belongsTo(tours, { as: "tour", foreignKey: "tour_id"});
  tours.hasMany(carts, { as: "carts", foreignKey: "tour_id"});
  carts.belongsTo(tours, { as: "org", foreignKey: "org_id"});
  tours.hasMany(carts, { as: "org_carts", foreignKey: "org_id"});
  report_tours.belongsTo(tours, { as: "tour", foreignKey: "tour_id"});
  tours.hasMany(report_tours, { as: "report_tours", foreignKey: "tour_id"});
  report_tours.belongsTo(tours, { as: "org", foreignKey: "org_id"});
  tours.hasMany(report_tours, { as: "org_report_tours", foreignKey: "org_id"});
  reviews.belongsTo(tours, { as: "tour", foreignKey: "tour_id"});
  tours.hasMany(reviews, { as: "reviews", foreignKey: "tour_id"});
  reviews.belongsTo(tours, { as: "org", foreignKey: "org_id"});
  tours.hasMany(reviews, { as: "org_reviews", foreignKey: "org_id"});
  tour_photos.belongsTo(tours, { as: "tour", foreignKey: "tour_id"});
  tours.hasMany(tour_photos, { as: "tour_photos", foreignKey: "tour_id"});

  return {
    account_avatars,
    accounts,
    admins,
    available_status,
    carts,
    countries,
    end_users,
    likes,
    locations,
    organizations,
    report_tour_evidences,
    report_tours,
    reviews,
    roles,
    tour_photos,
    tours,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
