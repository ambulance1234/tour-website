const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "carts",
    {
      cart_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "end_users",
          key: "account_id",
        },
      },
      tour_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "tours",
          key: "tour_id",
        },
      },
      org_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "tours",
          key: "org_id",
        },
      },
      tour_name: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: true,
      },
      number_tourists: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      booked_time: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      completed: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      },
      created_at: {
        type: DataTypes.DATE,
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "carts",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [
            { name: "cart_id" },
            { name: "user_id" },
            { name: "tour_id" },
            { name: "org_id" },
          ],
        },
        {
          name: "idx_booked_cart",
          using: "BTREE",
          fields: [{ name: "booked_time" }],
        },
        {
          name: "fk_cart_user",
          using: "BTREE",
          fields: [{ name: "user_id" }],
        },
        {
          name: "fk_cart_tour",
          using: "BTREE",
          fields: [{ name: "tour_id" }, { name: "org_id" }],
        },
      ],
    }
  );
};
