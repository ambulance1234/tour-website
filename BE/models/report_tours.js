const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report_tours', {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'end_users',
        key: 'account_id'
      }
    },
    tour_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tours',
        key: 'tour_id'
      }
    },
    org_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tours',
        key: 'org_id'
      }
    },
    reason: {
      type: DataTypes.STRING(1000),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'report_tours',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
          { name: "tour_id" },
          { name: "org_id" },
        ]
      },
      {
        name: "fk_rpt_tour",
        using: "BTREE",
        fields: [
          { name: "tour_id" },
          { name: "org_id" },
        ]
      },
    ]
  });
};
