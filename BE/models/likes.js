const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('likes', {
    review_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'reviews',
        key: 'user_id'
      }
    },
    review_tour_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'reviews',
        key: 'tour_id'
      }
    },
    review_org_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'reviews',
        key: 'org_id'
      }
    },
    upvoter_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'end_users',
        key: 'account_id'
      }
    }
  }, {
    sequelize,
    tableName: 'likes',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "review_user_id" },
          { name: "review_tour_id" },
          { name: "review_org_id" },
          { name: "upvoter_user_id" },
        ]
      },
      {
        name: "fk_like_user",
        using: "BTREE",
        fields: [
          { name: "upvoter_user_id" },
        ]
      },
    ]
  });
};
