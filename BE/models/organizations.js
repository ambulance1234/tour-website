const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('organizations', {
    account_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'accounts',
        key: 'account_id'
      }
    },
    org_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: "idx_orgname_org"
    },
    confirm_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    postal_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    business_license: {
      type: DataTypes.STRING(255),
      allowNull: true,
      unique: "idx_blicense_org"
    },
    warning: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'organizations',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "account_id" },
        ]
      },
      {
        name: "idx_orgname_org",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "org_name" },
        ]
      },
      {
        name: "idx_blicense_org",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "business_license" },
        ]
      },
    ]
  });
};
