const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "reviews",
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "end_users",
          key: "account_id",
        },
      },
      tour_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "tours",
          key: "tour_id",
        },
      },
      org_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: "tours",
          key: "org_id",
        },
      },
      number_stars: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      comment: {
        type: DataTypes.STRING(1000),
        allowNull: true,
      },
      upvote: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: 0,
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "reviews",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [
            { name: "user_id" },
            { name: "tour_id" },
            { name: "org_id" },
          ],
        },
        {
          name: "idx_star_rv",
          using: "BTREE",
          fields: [{ name: "number_stars" }],
        },
        {
          name: "fk_rev_tour",
          using: "BTREE",
          fields: [{ name: "tour_id" }, { name: "org_id" }],
        },
      ],
    }
  );
};
