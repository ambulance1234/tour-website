const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tours', {
    tour_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    org_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'organizations',
        key: 'account_id'
      }
    },
    location_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'locations',
        key: 'location_id'
      }
    },
    status_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1,
      references: {
        model: 'available_status',
        key: 'status_id'
      }
    },
    tour_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    price: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: true
    },
    number_nights: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    number_days: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    hours: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    introduce: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true,
    }
  }, {
    sequelize,
    tableName: 'tours',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tour_id" },
          { name: "org_id" },
        ]
      },
      {
        name: "fk_tour_acc",
        using: "BTREE",
        fields: [
          { name: "org_id" },
        ]
      },
      {
        name: "fk_tour_loc",
        using: "BTREE",
        fields: [
          { name: "location_id" },
        ]
      },
      {
        name: "fk_tour_status",
        using: "BTREE",
        fields: [
          { name: "status_id" },
        ]
      },
    ]
  });
};
