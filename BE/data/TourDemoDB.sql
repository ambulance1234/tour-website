CREATE TABLE `Account_Avatars`  (
  `account_id` int NOT NULL,
  `photo` varchar(255) NULL,
  PRIMARY KEY (`account_id`)
);

CREATE TABLE `Accounts`  (
  `account_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NULL,
  `role_id` int NULL,
  `country_id` int NULL,
  `created_at` datetime NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE INDEX `idx_email_acc`(`email`),
  UNIQUE INDEX `idx_phone_acc`(`phone`)
);

CREATE TABLE `Admins`  (
  `account_id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 NULL,
  `address` varchar(255) NULL,
  `gender` varchar(255) NULL,
  `age` int NULL,
  `secret_key` varchar(255) NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE INDEX `idx_username_adm`(`username`)
);

CREATE TABLE `Available_Status`  (
  `status_id` int NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) NULL,
  PRIMARY KEY (`status_id`)
);

CREATE TABLE `Carts`  (
  `cart_id` int NOT NULL,
  `user_id` int NOT NULL,
  `tour_id` int NOT NULL,
  `org_id` int NOT NULL,
  `tour_name` varchar(255) NULL,
  `price` decimal(10, 2) NULL,
  `number_tourists` int NULL,
  `booked_time` datetime NULL,
  `completed` bit NULL DEFAULT 0,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`cart_id`, `user_id`, `tour_id`, `org_id`),
  INDEX `idx_booked_cart`(`booked_time`)
);

CREATE TABLE `Countries`  (
  `country_id` int NOT NULL AUTO_INCREMENT,
  `country_code` varchar(255) NULL,
  `country_name` varchar(255) NULL,
  PRIMARY KEY (`country_id`)
);

CREATE TABLE `End_Users`  (
  `account_id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `rep_point` int NULL DEFAULT 0,
  `age` int NULL,
  `address` varchar(255) NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 NULL,
  `gender` varchar(255) NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE INDEX `idx_username_euser`(`username`)
);

CREATE TABLE `Likes`  (
  `review_user_id` int NOT NULL,
  `review_tour_id` int NOT NULL,
  `review_org_id` int NOT NULL,
  `upvoter_user_id` int NOT NULL,
  PRIMARY KEY (`review_user_id`, `review_tour_id`, `review_org_id`, `upvoter_user_id`)
);

CREATE TABLE `Locations`  (
  `location_id` int NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) NULL,
  `country_id` int NULL,
  PRIMARY KEY (`location_id`)
);

CREATE TABLE `Organizations`  (
  `account_id` int NOT NULL,
  `org_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `confirm_code` varchar(255) NULL,
  `address` varchar(255) NULL,
  `postal_code` varchar(255) NULL,
  `business_license` varchar(255) NULL,
  `warning` int NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE INDEX `idx_orgname_org`(`org_name`),
  UNIQUE INDEX `idx_blicense_org`(`business_license`)
);

CREATE TABLE `Report_Tour_Evidences`  (
  `user_id` int NOT NULL,
  `tour_id` int NOT NULL,
  `org_id` int NOT NULL,
  `description` varchar(255) NULL,
  PRIMARY KEY (`user_id`, `tour_id`, `org_id`)
);

CREATE TABLE `Report_Tours`  (
  `user_id` int NOT NULL,
  `tour_id` int NOT NULL,
  `org_id` int NOT NULL,
  `reason` varchar(1000) NULL,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`user_id`, `tour_id`, `org_id`)
);

CREATE TABLE `Reviews`  (
  `user_id` int NOT NULL,
  `tour_id` int NOT NULL,
  `org_id` int NOT NULL,
  `number_stars` int NULL,
  `comment` varchar(1000) NULL,
  `upvote` int NULL DEFAULT 0,
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  PRIMARY KEY (`user_id`, `tour_id`, `org_id`),
  INDEX `idx_star_rv`(`number_stars`)
);

CREATE TABLE `Roles`  (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NULL,
  PRIMARY KEY (`role_id`)
);

CREATE TABLE `Tour_Photos`  (
  `photo_id` int NOT NULL AUTO_INCREMENT,
  `tour_id` int NOT NULL,
  `description` varchar(255) NULL,
  `photo` varchar(255) NULL,
  PRIMARY KEY (`photo_id`, `tour_id`)
);

CREATE TABLE `Tours`  (
  `tour_id` int NOT NULL,
  `org_id` int NOT NULL,
  `location_id` int NOT NULL,
  `status_id` int NULL DEFAULT 1,
  `tour_name` varchar(255) NOT NULL,
  `price` decimal(10, 2) NULL,
  `number_nights` int NULL,
  `number_days` int NULL,
  `hours` varchar(255) NULL,
  `introduce` varchar(255) NULL,
  `description` varchar(1000) NULL,
  `created_at` datetime NULL,
  PRIMARY KEY (`tour_id`, `org_id`)
);

ALTER TABLE `Account_Avatars` ADD CONSTRAINT `fk_avt_acc` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`account_id`) ON DELETE CASCADE;
ALTER TABLE `Accounts` ADD CONSTRAINT `fk_acc_role` FOREIGN KEY (`role_id`) REFERENCES `Roles` (`role_id`);
ALTER TABLE `Accounts` ADD CONSTRAINT `fk_acc_ctr` FOREIGN KEY (`country_id`) REFERENCES `Countries` (`country_id`);
ALTER TABLE `Admins` ADD CONSTRAINT `fk_adm_acc` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`account_id`);
ALTER TABLE `Carts` ADD CONSTRAINT `fk_cart_user` FOREIGN KEY (`user_id`) REFERENCES `End_Users` (`account_id`) ON DELETE CASCADE;
ALTER TABLE `Carts` ADD CONSTRAINT `fk_cart_tour` FOREIGN KEY (`tour_id`, `org_id`) REFERENCES `Tours` (`tour_id`, `org_id`) ON DELETE CASCADE;
ALTER TABLE `End_Users` ADD CONSTRAINT `fk_end_acc` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`account_id`);
ALTER TABLE `Likes` ADD CONSTRAINT `fk_like_rev` FOREIGN KEY (`review_user_id`, `review_tour_id`, `review_org_id`) REFERENCES `Reviews` (`user_id`, `tour_id`, `org_id`) ON DELETE CASCADE;
ALTER TABLE `Likes` ADD CONSTRAINT `fk_like_user` FOREIGN KEY (`upvoter_user_id`) REFERENCES `End_Users` (`account_id`) ON DELETE CASCADE;
ALTER TABLE `Locations` ADD CONSTRAINT `fk_loc_ctr` FOREIGN KEY (`country_id`) REFERENCES `Countries` (`country_id`);
ALTER TABLE `Organizations` ADD CONSTRAINT `fk_org_acc` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`account_id`);
ALTER TABLE `Report_Tour_Evidences` ADD CONSTRAINT `fk_rptevd_rpt` FOREIGN KEY (`user_id`, `tour_id`, `org_id`) REFERENCES `Report_Tours` (`user_id`, `tour_id`, `org_id`) ON DELETE CASCADE;
ALTER TABLE `Report_Tours` ADD CONSTRAINT `fk_rpt_user` FOREIGN KEY (`user_id`) REFERENCES `End_Users` (`account_id`);
ALTER TABLE `Report_Tours` ADD CONSTRAINT `fk_rpt_tour` FOREIGN KEY (`tour_id`, `org_id`) REFERENCES `Tours` (`tour_id`, `org_id`) ON DELETE CASCADE;
ALTER TABLE `Reviews` ADD CONSTRAINT `fk_rev_user` FOREIGN KEY (`user_id`) REFERENCES `End_Users` (`account_id`);
ALTER TABLE `Reviews` ADD CONSTRAINT `fk_rev_tour` FOREIGN KEY (`tour_id`, `org_id`) REFERENCES `Tours` (`tour_id`, `org_id`) ON DELETE CASCADE;
ALTER TABLE `Tour_Photos` ADD CONSTRAINT `fk_tpt_tour` FOREIGN KEY (`tour_id`) REFERENCES `Tours` (`tour_id`) ON DELETE CASCADE;
ALTER TABLE `Tours` ADD CONSTRAINT `fk_tour_acc` FOREIGN KEY (`org_id`) REFERENCES `Organizations` (`account_id`) ON DELETE CASCADE;
ALTER TABLE `Tours` ADD CONSTRAINT `fk_tour_loc` FOREIGN KEY (`location_id`) REFERENCES `Locations` (`location_id`);
ALTER TABLE `Tours` ADD CONSTRAINT `fk_tour_status` FOREIGN KEY (`status_id`) REFERENCES `Available_Status` (`status_id`);

