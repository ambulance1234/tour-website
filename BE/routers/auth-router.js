const controller = require('../controllers/account_controllers/auth-controller');
// const authService = require('../services/auth-services');

const express = require('express');
const resourceFilter = require('../midlewares/resources-filter');
const verifyAccess = require('../midlewares/authorization-filter');
const route = express.Router();

route.post('/login', controller.login);
route.get('/logout', verifyAccess.acceptAllRoles, controller.logout);

module.exports = route;