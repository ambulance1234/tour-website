const accountBaseController = require("../controllers/account_controllers/account-controller");
const userController = require("../controllers/account_controllers/user-controller");
const userCartController = require("../controllers/account_controllers/user-cart-controller");
const orgController = require("../controllers/account_controllers/org-controller");
const express = require("express");
const resourceFilter = require("../midlewares/resources-filter");
const authorization = require("../midlewares/authorization-filter");
const route = express.Router();

route.use("/", resourceFilter);
route.get("/", authorization.acceptAdminOnly, accountBaseController.getAll);
route.post("/register", accountBaseController.register);

route.get(
  "/users",
  authorization.acceptAdminOnly,
  userController.getAllEndUsers
);
route.get("/users/ask", userController.getEndUserByIdOrName);
route.post("/users/register", userController.userRegister);
route.put(
  "/users/update",
  authorization.acceptUserOnly,
  userController.updateUserInfo
);
route.get(
  "/users/cart",
  authorization.acceptUserOnly,
  userCartController.getCartOfUser
);
route.get(
  "/users/cart/detail",
  authorization.acceptUserOnly,
  userCartController.getDetailOfBookedTour
);
route.post(
  "/users/book-tour",
  authorization.acceptUserOnly,
  userCartController.bookTheTourFromUser
);
route.delete(
  "/users/cancel-tour",
  authorization.acceptUserOnly,
  userCartController.cancelBookedTour
);

route.get("/orgs", authorization.acceptAdminOnly, orgController.getAllOrgs);
route.post("/orgs/ask", orgController.getOrgByIdOrBLicense);
route.post("/orgs/register", orgController.orgRegister);
route.post(
  "/orgs/updateCommonInfo",
  authorization.acceptOrgOnly,
  orgController.updateOrgInfo
);
route.post(
  "/orgs/updateCode",
  authorization.acceptOrgOnly,
  orgController.changeConfirmCode
);
route.post("/orgs/search", orgController.searchOrgs);

module.exports = route;
