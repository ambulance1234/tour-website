const adminController = require("../controllers/account_controllers/admin-controller");
const authorized = require("../midlewares/authorization-filter");

const express = require("express");
const route = express.Router();

route.use(authorized.acceptAdminOnly);

route.delete('/delete-user', adminController.deleteEndUserAction);
route.delete('/delete-org', adminController.deleteOrgAction);
route.get('/all', adminController.getAllAdmins);
route.post('/register', adminController.adminRegister);

module.exports = route;
