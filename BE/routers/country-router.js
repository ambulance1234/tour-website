const express = require('express');
const route = express.Router();

const countryController = require('../controllers/country-controller');

const locationController = require('../controllers/location-controller');
const authorized = require('../midlewares/authorization-filter');

route.get('/', countryController.getAll);
route.get('/ask', countryController.getByIdOrCode);

route.get('/locations', locationController.getAll);
route.get('/locations/askforlocation', locationController.getLocationsByName);
route.get('/locations/askforcountry', locationController.getLocationsByCountryCode);
route.post('/locations/create', authorized.acceptAdminOrg, locationController.createNewLocation);

module.exports = route;