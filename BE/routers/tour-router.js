const express = require("express");
const route = express.Router();
const tourController = require("../controllers/tour_controllers/tour_controller");
const authorized = require("../midlewares/authorization-filter");

const tourImageController = require("../controllers/tour_controllers/tour-image-controller");
const reviewController = require("../controllers/tour_controllers/review_controller");
const cartController = require("../controllers/account_controllers/user-cart-controller");

route.get("/org", authorized.acceptAdminOrg, tourController.getToursOfOrg);
route.get("/detail", tourController.getDetailTour);
route.get("/search/location", tourController.searchToursWithLocation);
route.get("/search/name", tourController.searchTourByName);
route.get("/top", tourController.getTopRaingTours);

route.post("/create", authorized.acceptOrgOnly, tourController.postNewTour);
route.put("/update", authorized.acceptOrgOnly, tourController.updateTourPost);
route.delete("/delete", authorized.acceptOrgOnly, tourController.deleteTours);

route.get(
  "/org/number-of-orderers",
  authorized.acceptOrgOnly,
  tourController.getListOfNumberOrderersOfTheTour
);
route.get(
  "/org/tourists",
  authorized.acceptOrgOnly,
  tourController.getListOfTouristOfTourByDate
);
route.post(
  "/org/tourists/mark-completed",
  authorized.acceptOrgOnly,
  cartController.updateCompletedCart
);
route.delete(
  "/org/disable",
  authorized.acceptOrgOnly,
  tourController.disableMultiToursOfOrg
);

route.get("/images", tourImageController.getImage);
route.post("/images", tourImageController.uploadImg);

route.get("/reviews", reviewController.getReviewsOfTour);
route.get("/reviews/totalnumber", reviewController.getNumberReviewsOfTour);
route.post(
  "/reviews/create",
  authorized.acceptUserOnly,
  reviewController.postNewReview
);
route.put(
  "/reviews/upvote",
  authorized.acceptUserOnly,
  reviewController.upvoteReview
);

module.exports = route;
