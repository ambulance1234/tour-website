var Sequelize = require("sequelize");
require("dotenv").config();

console.log(process.env.MODE);
console.log(process.env.MODE === "DEVELOP" ? "Run LocalDB" : "Run RemoteDB");

var sequelizeLocal = new Sequelize("tourdemoinnodb", "bob", "12345678@a", {
  host: "localhost",
  port: 3306,
  dialect: "mysql",
  // timezone: 'Asia/Saigon'
  define: {
    timestamps: false,
  },
  dialectOptions: {
    //  useUTC: false,
    dateStrings: true,
    typeCast: function (field, next) {
      if (field.type === "DATETIME") {
        return field.string();
      }
      return next();
    },
  },
  timezone: "+07:00",
});

var sequelizeOnHost = new Sequelize(
  "db_a84f19_tourobj",
  "a84f19_tourobj",
  "01679751807Ph",
  {
    host: "MYSQL5025.site4now.net",
    port: 3306,
    dialect: "mysql",
    // timezone: 'Asia/Saigon'
    define: {
      timestamps: false,
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialectOptions: {
      //  useUTC: false,
      dateStrings: true,
      typeCast: function (field, next) {
        if (field.type === "DATETIME") {
          return field.string();
        }
        return next();
      },
    },
    timezone: "+07:00",
  }
);

module.exports =
  process.env.MODE === "DEVELOP" ? sequelizeLocal : sequelizeOnHost;
