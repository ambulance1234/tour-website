const sequelize = require("./sequelize-config");
const initModels = require("../models/init-models");
const models = initModels(sequelize);

module.exports = models;