const NotFound = {
    status: 404,
    message: 'Not Found'    
}
const Ok = {
    status: 200,
    message: 'Successful'
}
const ServerError = {
    status: 500,
    message: 'Server Internal Error'
}
const Created = {
    status: 201,
    message: 'Successfully Created'
}
const BadRequest = {
    status: 400,
    message: 'Bad Request'
}
const NoContent = {
    status: 204,
    message: 'No Content'
}
const UnsupportedMediaType = {
    status: 415,
    message: "Unsupported Media Type"
}
const UnAuthorized  = {
    status: 401,
    message: "Unauthorized"
}
const Conflict  = {
    status: 409,
    message: "Conflict - Existed"
}
module.exports = { NotFound, Ok, ServerError, Created, BadRequest, NoContent, UnsupportedMediaType, UnAuthorized, Conflict }

