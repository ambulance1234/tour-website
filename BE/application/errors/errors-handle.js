const commonErrors = require('./common-errors');
const StatusCode = require("../status-code");
const registerErrorsCatched = (res, error) => {
  switch (error.name) {
    case commonErrors.DataExistedError:
      return res
        .status(StatusCode.Conflict.status)
        .json({ message: "Account is Existed" });
    case commonErrors.InvalidRoleError:
      return res
        .status(StatusCode.BadRequest.status)
        .json({ message: "Invalid Role" });
    case commonErrors.InvalidInformationError:
      return res
        .status(StatusCode.BadRequest.status)
        .json({ message: "Invalid Information" });
    default:
      return res.status(StatusCode.ServerError.status).json({ error: error });
  }
};

const updatedErrorCatched = (res, error) => {
  switch (error.name) {
    case commonErrors.DataNotExistedError:
      return res
        .status(StatusCode.BadRequest.status)
        .json({ message: "Given Data is not existed" });
    case commonErrors.InformationNotMatchedError:
      return res
        .status(StatusCode.UnAuthorized.status)
        .json({ message: "Credentials did not match" });
    default:
      return res
        .status(StatusCode.ServerError.status)
        .json({ message: error });
  }
}

module.exports = {
    registerErrorsCatched,
    updatedErrorCatched
};
