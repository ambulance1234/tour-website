const commonErrors = {
  //information not meet the requirements
  InvalidInformationError: "InvalidInformationError",
  //Data (record) existed in database
  DataExistedError: "DataExistedError",
  //Data (record) NOT existed in database
  DataNotExistedError: "DataNotExistedError",
  //Role is invalid or not existed in database
  InvalidRoleError: "InvalidRoleError",
  //Data Type not meet requirements: input is string but using number
  UnsupportedTypeError: "UnsupportedTypeError",
  //Given Information did not matched with Original Information
  InformationNotMatchedError: "InformationNotMatchedError"
};

module.exports = commonErrors;
