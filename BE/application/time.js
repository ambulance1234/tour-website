const getNow = () => {
  const now = new Date();
  let dd = String(now.getDate()).padStart(2, "0");
  let mm = String(now.getMonth() + 1).padStart(2, "0"); //January is 0!
  let yyyy = now.getFullYear();
  let HH = String(now.getHours()).padStart(2, "0");
  let MM = String(now.getMinutes()).padStart(2, "0");
  let SS = String(now.getSeconds()).padStart(2, "0");
  let date = `${yyyy}-${mm}-${dd} ${HH}:${MM}:${SS}`;
  console.log(`Time now: ${date}`);
  return date;
};

const dateToString = (date) => {
  let dd = String(date.getDate()).padStart(2, "0");
  let mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
  let yyyy = date.getFullYear();
  return `${yyyy}-${mm}-${dd}`;
};

const hourToString = (date) => {
  let HH = String(now.getHours()).padStart(2, "0");
  let MM = String(now.getMinutes()).padStart(2, "0");
  let SS = String(now.getSeconds()).padStart(2, "0");
  return `${HH}:${MM}:${SS}`;
};

const compareTime = (time1, time2) => {
  if (time1 > time2) {
    //time1 is after time2
    return 1;
  } else if (time1 < time2) {
    //time1 is before time2
    return -1;
  } else {
    return 0;
  }
};

const totalDateFromTo = (from, to) => {
  const totalTime =
    Math.round(to.getTime() - from.getTime()) / (1000 * 3600 * 24);
  const totalDate = totalTime.toFixed(0);
  return totalDate;
};

const newDateFromHours = (date, hours) => {
  const miliseconds = hours * 3600000;
  let newDate = date.getTime() + miliseconds;
  return new Date(newDate);
};

// console.log(new Date("2022-03-09"));

// console.log(totalTimeFromTo(new Date(), new Date("2022-03-18")));

module.exports = { getNow, compareTime, totalDateFromTo, newDateFromHours };
